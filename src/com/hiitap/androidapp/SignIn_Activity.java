package com.hiitap.androidapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hiitap.androidapp.webservices.WSConnector;

public class SignIn_Activity extends Activity {
	Button btnsin, btnsiup;
	EditText edtId, edtPwd;
	static boolean back_show = false;
	String username, password, token, response, uuid, getSession;
	SharedPreferences sharedPreferences;
	Editor editor;
	JSONObject jsonobject;
	ProgressDialog pDialog;
	ImageButton img_back;
	ImageView imageView;
	TextView txt_forgotpwd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signin);
		sharedPreferences = getSharedPreferences(AppConstant.HII_TAP,
				Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(this);

		username = sharedPreferences.getString(AppConstant.USERNAME, "");
		password = sharedPreferences.getString(AppConstant.PASSWORD, "");
		getInt();
	}

	private void getInt() {
		// TODO Auto-generated method stub
		edtId = (EditText) findViewById(R.id.edt_signin_Email);
		edtId.setCursorVisible(true);
		edtPwd = (EditText) findViewById(R.id.edt_signin_pwd);
		edtPwd.setCursorVisible(true);
		btnsin = (Button) findViewById(R.id.btn_signin);
		btnsiup = (Button) findViewById(R.id.btn_signUP);
		img_back = (ImageButton) findViewById(R.id.back_button_welcome);
		imageView = (ImageView) findViewById(R.id.img_tap);
		txt_forgotpwd = (TextView) findViewById(R.id.txt_FrgtPwd);
		btnsiup.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ii = new Intent(SignIn_Activity.this,
						SignUp_Activity.class);
				startActivity(ii);
				finish();
			}
		});
		btnsin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				username = edtId.getText().toString().trim();
				password = edtPwd.getText().toString().trim();
				boolean receive = validation(username);
				if (receive) {
					new ExcecuteTask().execute(AppConstant.BASE_URL,AppConstant.LOGIN, username,
							password);
				}
//				else {
//					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
//							SignIn_Activity.this);
//					alertDialog
//							.setMessage("Invalid email address or password.")
//							.setCancelable(false)
//							.setPositiveButton("Ok",
//									new DialogInterface.OnClickListener() {
//
//										@Override
//										public void onClick(DialogInterface dialog,
//												int which) {
//											// TODO Auto-generated method stub
//											dialog.dismiss();
//											//
//										}
//									});
//					AlertDialog dialog = alertDialog.create();
//					dialog.show();
//				}
//
			}
		});
		img_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignIn_Activity.this,
						WelcomePage_Activity.class);
				startActivity(i);
				finish();

			}
		});
		imageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignIn_Activity.this,
						WelcomePage_Activity.class);
				startActivity(i);
				finish();
			}
		});

		txt_forgotpwd.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignIn_Activity.this,
						ForgotPassword.class);
				startActivity(i);

			}
		});

	}

	private boolean validation(String username) {
		// TODO Auto-generated method stub
		String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";

		if (username.matches("")) {
			edtId.setError("Please enter your email address");
			return false;
		}
		if (!username.matches(emailPattern)) {
			edtId.setError("Email address doesn't exist");
			return false;
		}

		return true;
	}

	

	class ExcecuteTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result_authenticate = WSConnector.authenticate(params[2],
					params[3]);

			return result_authenticate;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			super.onPreExecute();

			pDialog.setMessage("Loading...");

			pDialog.show();

		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);

			pDialog.dismiss();
			updateUI(jsonobjectString);
			new ExecuteGetSession().execute(AppConstant.BASE_URL,
					AppConstant.GET_SESSION);
		}

	}

	public void updateUI(String jsonobjectString) {
		JSONArray jsonArrobject = null;
		try {
			jsonArrobject = new JSONArray(jsonobjectString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonobject = null;
		try {
			jsonobject = jsonArrobject.getJSONObject(0);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if (!jsonobject.has("token")) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						SignIn_Activity.this);
				alertDialog
						.setMessage("Invalid email address or password.")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
										//
									}
								});
				AlertDialog dialog = alertDialog.create();
				dialog.show();

				return;
			}

			token = jsonobject.getString("token");

			sharedPrefernces();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class ExecuteGetSession extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result = WSConnector.postSession(token);
			return result;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);

			getUpdatedSession(jsonobjectString);

		}

	}

	public void getUpdatedSession(String jsonobjectString) {
		// TODO Auto-generated method stub
		JSONArray jsonArrobject = null;
		try {
			jsonArrobject = new JSONArray(jsonobjectString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonobject = null;
		try {
			jsonobject = jsonArrobject.getJSONObject(0);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			uuid = jsonobject.getString("person");

			shared_Prefernces();
			Intent i = new Intent(SignIn_Activity.this, MainActivity.class);
			startActivity(i);
			finish();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();

		editor.putString(AppConstant.USERNAME, username);
		editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();

	}

	public void shared_Prefernces() {
		editor = sharedPreferences.edit();

		editor.putString(AppConstant.USERNAME, username);
		editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();

	}

}
