package com.hiitap.androidapp.home;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.SignIn_Activity;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.modal.CitiesModal;
import com.hiitap.androidapp.modal.CountryModel;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.modal.UserModal;
import com.hiitap.androidapp.profile.LikeDetails_fragment;
import com.hiitap.androidapp.profile.ProfileFragment;
import com.hiitap.androidapp.ui.SegmentedGroup;
import com.hiitap.androidapp.util.Utility;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class HomeFragment extends Fragment implements
		RadioGroup.OnCheckedChangeListener {
	ExpandableListAdapter listAdapter;
	ExpandableListView expandableListView;
	JSONArray jsonArray;
	ListView listView_mostLiked, listView_mostThanked;

	Adapter_MostThanked adapterMostThanked;
	Adater_MostLiked adapterMostLiked;

	SharedPreferences sharedPreferences;
	Editor editor;
	Button btn_logout, btn_refresh, btn_about;

	JSONArray mJsonArray;
	String uuid, token, success;

	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;
	SegmentedGroup segmentedgrp;

	private GoogleMap mMap;
	private MapView mMapView;
	boolean isClicked = false;
	View rootView;
	List<MostThankedModel> mostThankedModels;
	List<UserModal> userModals;
	UserModal modal;
	SupportMapFragment mSupportMapFragment;
	JSONObject jsonobject;
	ProgressDialog pDialog;
	RadioButton radioButton1, radioButton2, radioButton3;
	String str_con, str_cur, response, post, counter, m_uuid;
	List<CountryModel> list_of_countryModels;
	ViewHolder holder;
	CountryModel countryModel;
	List<CitiesModal> listcitiesModels;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		pDialog = new ProgressDialog(getActivity());
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		//new ExecuteMessgaeCount().execute();
		preparedata();
		userModals = new ArrayList<UserModal>();
		mostThankedModels = new ArrayList<MostThankedModel>();

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_home, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		SegmentedGroup segmented4 = (SegmentedGroup) rootView
				.findViewById(R.id.segmented4);
		segmented4.setTintColor(getResources().getColor(
				R.color.radio_button_selected_color));
		radioButton1 = (RadioButton) rootView.findViewById(R.id.button41);
		radioButton1.setChecked(true);
		radioButton2 = (RadioButton) rootView.findViewById(R.id.button42);
		radioButton3 = (RadioButton) rootView.findViewById(R.id.button43);

		segmented4.setOnCheckedChangeListener(this);

		expandableListView = (ExpandableListView) rootView
				.findViewById(R.id.expandableListView1);
		expandableListView.setVisibility(View.VISIBLE);
		expandableListView.setGroupIndicator(null);

		btn_refresh = (Button) rootView.findViewById(R.id.btn_Refesh);
		btn_about = (Button) rootView.findViewById(R.id.btn_About);
		btn_logout = (Button) rootView.findViewById(R.id.btn_Logout);

		listView_mostThanked = (ListView) rootView
				.findViewById(R.id.listview_thanked);

		listView_mostThanked.setVisibility(View.GONE);
		listView_mostLiked = (ListView) rootView
				.findViewById(R.id.listview_mostLiked);
		listView_mostLiked.setVisibility(View.GONE);
		listView_mostThanked.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final UserModal modal = userModals.get(position);
				m_uuid = modal.getUUID();
				ProfileFragment fragment = new ProfileFragment();
				Bundle savedInstanceState = new Bundle();
				savedInstanceState.putString("UUID_PASS", m_uuid);
				fragment.setArguments(savedInstanceState);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);
			}

		});

		btn_logout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						getActivity());
				alertDialog
						.setTitle("Logout")
						.setMessage("Are you sure you want to logout?")
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										new ExecuteLogoutTask()
												.execute(AppConstant.BASE_URL,
														AppConstant.LOGOUT,
														uuid, token);
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
									}
								});
				AlertDialog dialog = alertDialog.create();
				dialog.show();

			}

		});

		btn_refresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Toast.makeText(getActivity(), "Refreshing..",
						Toast.LENGTH_SHORT).show();
			}
		});

		btn_about.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent iAbout = new Intent(getActivity(), AboutActivity.class);
				startActivity(iAbout);

			}
		});
		radioButton2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (userModals.size() == 0) {
					new ExcecuteTask_Mostthanked().execute(
							AppConstant.BASE_URL,
							AppConstant.GET_POPULAR_PEOPLE, uuid, token);
				}

			}
		});
		radioButton3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mostThankedModels.size() == 0) {
					new ExecuteTask_Mostliked().execute(AppConstant.BASE_URL,
							AppConstant.GET_POPULAR_POSTS, uuid, token);
				}

			}
		});

//		 MapsInitializer.initialize(getActivity());
//		 mMapView = (MapView) rootView.findViewById(R.id.map);
//		 mMapView.onCreate(savedInstanceState);
//		
//		 setUpMapAndUserCurrentLocation();
		return rootView;

	}

//	 private void setUpMapAndUserCurrentLocation() {
//	 if (mMap == null) {
//	 LatLng clatLng;
//	 mMap = mMapView.getMap();
//	 mMap.getUiSettings().setMyLocationButtonEnabled(false);
//	 mMap.setMyLocationEnabled(true);
//	
//	 // // Updates the location and zoom of the MapView
//	 Location loc = mMap.getMyLocation();
//	 if (loc != null) {
//	 //
//	 clatLng = new LatLng(loc.getLatitude(), loc.getLongitude());
//	 } else {
//	 clatLng = new LatLng(49.2756d, -123.12717d);
//	 }
//	 mMap.moveCamera(CameraUpdateFactory.newLatLng(clatLng));
//	 CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
//	 clatLng, 12);
//	 mMap.animateCamera(cameraUpdate);
//	
//	 }
//	
//	 }
//	
//	 @Override
//	 public void onResume() {
//	 super.onResume();
//	 mMapView.onResume();
//	 }
//	
//	 @Override
//	 public void onPause() {
//	 mMapView.onPause();
//	 super.onPause();
//	 mMap = null;
//	 }
//	
//	 @Override
//	 public void onDestroy() {
//	 mMapView.onDestroy();
//	 super.onDestroy();
//	 }
//	
//	 @Override
//	 public void onLowMemory() {
//	 super.onLowMemory();
//	 mMapView.onLowMemory();
//	 }
//	
//	 @Override
//	 public void onSaveInstanceState(Bundle outState) {
//	 super.onSaveInstanceState(outState);
//	 mMapView.onSaveInstanceState(outState);
//	 }

	private void preparedata() {
		// TODO Auto-generated method stub
		
		new ExecuteCountry_Task().execute(AppConstant.BASE_URL,
				AppConstant.GET_POPULAR_COUNTRIES);

	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		switch (checkedId) {
		case R.id.button41:
			radioButton1.setSelected(true);
			initTopCountriesList();

			return;
		case R.id.button42:
			initMostThankedList();

			return;
		case R.id.button43:

			initMostLikedList();

			return;

		}
	}

	private void initMostThankedList() {
		expandableListView.setVisibility(View.GONE);
		listView_mostLiked.setVisibility(View.GONE);
		listView_mostThanked.setVisibility(View.VISIBLE);

	}

	private void initMostLikedList() {
		expandableListView.setVisibility(View.GONE);
		listView_mostLiked.setVisibility(View.VISIBLE);
		listView_mostThanked.setVisibility(View.GONE);

	}

	private void initTopCountriesList() {

		expandableListView.setVisibility(View.VISIBLE);
		listView_mostLiked.setVisibility(View.GONE);
		listView_mostThanked.setVisibility(View.GONE);

	}
	
//	class ExecuteMessgaeCount extends AsyncTask<String, Integer, String>{
//
//		@Override
//		protected String doInBackground(String... params) {
//			// TODO Auto-generated method stub
//			String result = WSConnector.GetMessageCount();
//			
//			return result;
//		}
//		@Override
//		protected void onPostExecute(String jsonobjectString) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(jsonobjectString);
//			//updateMessageCount(jsonobjectString);
//			
//			
//		}
//		private void updateMessageCount(String jsonobjectString) {
//			// TODO Auto-generated method stub
//			
//		}
//		
//	}

	class Adapter_MostThanked extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<UserModal> userModals;

		public Adapter_MostThanked(Context context, List<UserModal> userModals) {
			this.context = context;
			this.userModals = userModals;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return userModals.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.activity_listview_imagetext, null);

				holder = new ViewHolder();

				holder.txt_name = (TextView) convertView
						.findViewById(R.id.txt_name);
				holder.txt_received = (TextView) convertView
						.findViewById(R.id.txt_received);
				holder.image_profile = (ImageView) convertView
						.findViewById(R.id.flag);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final UserModal list = userModals.get(position);
			holder.txt_name.setText((list.getFirst_name()) + " "
					+ (list.getLast_name()));
			holder.txt_received.setText("TAPs Received:" + " "
					+ list.getReceived());
			Picasso.with(getActivity())
					.load("https://www.hiitap.com/p/" + list.getUUID())
					.into(holder.image_profile);

			return convertView;

		}

	}

	class Adater_MostLiked extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public Adater_MostLiked(Context context,
				List<MostThankedModel> mostThankedModels) {
			this.context = context;
			this.mostThankedModels = mostThankedModels;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mostThankedModels.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.activity_most_liked,
						null);

				holder = new ViewHolder();

				holder.txt_large = (TextView) convertView
						.findViewById(R.id.txtLArgeText);
				holder.txt_comment = (TextView) convertView
						.findViewById(R.id.txt_comments);
				holder.txt_date = (TextView) convertView
						.findViewById(R.id.txt_time);
				holder.img_flag = (ImageView) convertView
						.findViewById(R.id.flag);
				holder.txt_date = (TextView) convertView
						.findViewById(R.id.txt_time);
				holder.txt_commentCount = (TextView) convertView
						.findViewById(R.id.txtcomments_count);
				holder.txt_likeCount = (TextView) convertView
						.findViewById(R.id.likes_txt);
				holder.btn_heart = (Button) convertView
						.findViewById(R.id.btn_hearticon);
				holder.btn_Comment = (Button) convertView
						.findViewById(R.id.btn_commentimg);
				holder.txtcomment = (TextView) convertView
						.findViewById(R.id.txtcomments_count);
				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);

			holder.txt_large.setText(listDetailsModel.getFromUser()
					.getFirst_name()
					+ " "
					+ listDetailsModel.getFromUser().getLast_name()
					+ " "
					+ "sent"
					+ " "
					+ listDetailsModel.getAction()
					+ " "
					+ "to"
					+ " "
					+ listDetailsModel.getToUser().getFirst_name()
					+ " "
					+ listDetailsModel.getToUser().getLast_name());

			holder.txt_comment.setText(listDetailsModel.getMessage());
			Picasso.with(getActivity())
					.load("https://www.hiitap.com/p/"
							+ listDetailsModel.getToUser().getUUID())
					.into(holder.img_flag);

			String time = Utility.convertTime(listDetailsModel.getTime());
			holder.txt_date.setText(time);

			holder.txt_commentCount.setText("Comments " + " "
					+ listDetailsModel.getComment_count());

			holder.txt_likeCount.setText("Like " + " "
					+ listDetailsModel.getLike_count());

			holder.txt_likeCount.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					String firstUUID = listDetailsModel.getUUID();
					String mString_like = listDetailsModel.getLike_count();

					LikeDetails_fragment details_fragment = new LikeDetails_fragment();
					Bundle savedInstanceState = new Bundle();
					savedInstanceState.putString("UUID_PASS", firstUUID);
					savedInstanceState.putString("LIKE", mString_like);
					details_fragment.setArguments(savedInstanceState);
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(details_fragment, true);
				}

			});

			if (listDetailsModel.getLiked().matches("true")) {
				holder.btn_heart.setBackgroundResource(R.drawable.heart);

			} else {
				holder.btn_heart.setBackgroundResource(R.drawable.heart1);
			}

			holder.btn_heart.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String firstUUID = listDetailsModel.getUUID();
					String postTo = listDetailsModel.getToUser().getUUID();
					String like = listDetailsModel.getLiked();

					String txt_ = listDetailsModel.getLike_count();
					String like_status = "false";
					if (like.equals("false")) {
						like_status = "true";

						mostThankedModels.get(position).setLiked(like_status);

						int myNum = Integer.parseInt(txt_) + 1;
						// holder.txtlike.setText("Like"+" "+myNum);
						mostThankedModels.get(position).setLike_count(
								String.valueOf(myNum));

						notifyDataSetChanged();
						new ExcecuteLikePosts().execute(AppConstant.BASE_URL,
								AppConstant.LIKE_POSTS, token, firstUUID,
								postTo, like_status);

					} else if (like.equals("true")) {

						like_status = "false";
						// v.setBackgroundResource(R.drawable.heart1);
						mostThankedModels.get(position).setLiked(like_status);

						int myNum = Integer.parseInt(txt_) - 1;
						// holder.txtlike.setText("Like"+" "+myNum);
						mostThankedModels.get(position).setLike_count(
								String.valueOf(myNum));

						// holder.txtlike.setText("Like"+" "+myNum1);
						notifyDataSetChanged();
						new ExcecuteLikePosts().execute(AppConstant.BASE_URL,
								AppConstant.LIKE_POSTS, token, firstUUID,
								postTo, like_status);
					}
				}
			});

			holder.btn_Comment.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String getUuid = listDetailsModel.getUUID();
					String mString_comment = listDetailsModel
							.getComment_count();
					String person_uuid = listDetailsModel.getToUser().getUUID();
					Comment_Fragment comment_Fragment = new Comment_Fragment();
					Bundle savedInstanceState = new Bundle();
					savedInstanceState.putString("UUID_PASS", getUuid);
					savedInstanceState.putString("COMMENT", mString_comment);
					savedInstanceState.putString("PERSON_UUID", person_uuid);
					comment_Fragment.setArguments(savedInstanceState);
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(comment_Fragment, true);
				}
			});

			holder.txtcomment.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String getUuid = listDetailsModel.getUUID();
					String mString_comment = listDetailsModel
							.getComment_count();
					String person_uuid = listDetailsModel.getToUser().getUUID();

					Comment_Fragment comment_Fragment = new Comment_Fragment();
					Bundle savedInstanceState = new Bundle();
					savedInstanceState.putString("UUID_PASS", getUuid);
					savedInstanceState.putString("COMMENT", mString_comment);
					savedInstanceState.putString("PERSON_UUID", person_uuid);
					comment_Fragment.setArguments(savedInstanceState);
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(comment_Fragment, true);
				}
			});
			return convertView;

		}
	}

	static class ViewHolder {
		TextView txt_name, txt_received, txtlike, txtcomment, txt_large,
				txt_comment, txt_date, txt_likeCount, txt_commentCount;
		Button btn_heart, btn_Comment;
		ImageView image_profile, img_flag;

	}

	class ExecuteLogoutTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_out = WSConnector.getLogout(params[2], params[3]);
			return result_out;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pDialog.setMessage("Logging out...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			updateBackgroud(jsonobjectString);

			// Intent i = new Intent(getActivity(), SignIn_Activity.class);
			// startActivity(i);

		}

		private void updateBackgroud(String jsonobjectString) {
			// TODO Auto-generated method stub
			JSONArray jsonArrobject = null;
			try {
				jsonArrobject = new JSONArray(jsonobjectString);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject jsonobject = null;
			try {
				jsonobject = jsonArrobject.getJSONObject(0);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (jsonobject.has("success")) {

					if (jsonobject.getString("success").equals("ok")) {
						clearPrefernces();

						Intent i = new Intent(getActivity(),
								SignIn_Activity.class);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i);
						getActivity().finish();
						
					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	class ExcecuteTask_Mostthanked extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_getPopularpeople = WSConnector.getPopularPeople(
					params[2], params[3]);
			return result_getPopularpeople;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			userModals = update_MostthankedUI(jsonobjectString);
			adapterMostThanked = new Adapter_MostThanked(getActivity(),
					userModals);
			listView_mostThanked.setAdapter(adapterMostThanked);
		}

		public List<UserModal> update_MostthankedUI(String jsonobjectString) {
			// TODO Auto-generated method stub
			List<UserModal> userModals = new ArrayList<UserModal>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					modal = new UserModal();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);

					String uuid = jsonObject.getString("uuid");
					String firstname = jsonObject.getString("first_name");
					String lastname = jsonObject.getString("last_name");
					String received = jsonObject.getString("received");

					modal.setUUID(uuid);
					modal.setFirst_name(firstname);
					modal.setLast_name(lastname);
					modal.setReceived(received);
					userModals.add(modal);
				}
			} catch (JSONException e) {

			}
			return userModals;
		}

	}

	class ExecuteTask_Mostliked extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_getPopularposts = WSConnector.getPopularPosts(
					params[2], params[3]);
			return result_getPopularposts;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			mostThankedModels = updateUI_PopularPosts(jsonobjectString);
			adapterMostLiked = new Adater_MostLiked(getActivity(),
					mostThankedModels);
			listView_mostLiked.setAdapter(adapterMostLiked);
		}

	}

	public List<MostThankedModel> updateUI_PopularPosts(String jsonobjectString) {
		// TODO Auto-generated method stub
		List<MostThankedModel> mostThankedModels = new ArrayList<MostThankedModel>();
		try {
			mJsonArray = new JSONArray(jsonobjectString);
			Log.e("jsonobjectString------", "" + jsonobjectString);

			for (int i = 0; i < mJsonArray.length(); i++) {
				MostThankedModel thankedModel = new MostThankedModel();
				UserModal userModal = new UserModal();
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				String uuid = jsonObject.getString("uuid");
				String action = jsonObject.getString("action");
				String message = jsonObject.getString("message");
				String like_count = jsonObject.getString("like_count");
				String comment_count = jsonObject.getString("comment_count");
				String time = jsonObject.getString("time");
				String liked = jsonObject.getString("liked");
				// Todo get remaining items

				thankedModel.setUUID(uuid);
				thankedModel.setAction(action);
				thankedModel.setMessage(message);
				thankedModel.setLike_count(like_count);
				thankedModel.setComment_count(comment_count);
				thankedModel.setTime(time);
				thankedModel.setLiked(liked);
				UserModal toUserModel = new UserModal();
				JSONObject jsonInnerToOject = jsonObject.getJSONObject("to");
				String uuidtoInner = jsonInnerToOject.getString("uuid");
				String fNameTo = jsonInnerToOject.getString("first_name");
				String lNameTo = jsonInnerToOject.getString("last_name");
				// Todo get remaining items
				toUserModel.setUUID(uuidtoInner);
				toUserModel.setFirst_name(fNameTo);
				toUserModel.setLast_name(lNameTo);

				thankedModel.setToUser(toUserModel);

				// Todo set usermodel for from
				UserModal fromUserModal = new UserModal();
				JSONObject jsonfromInner = jsonObject.getJSONObject("from");
				String uuidFrom = jsonfromInner.getString("uuid");
				String fnameFrom = jsonfromInner.getString("first_name");
				String lNameFrom = jsonfromInner.getString("last_name");
				fromUserModal.setUUID(uuidFrom);
				fromUserModal.setFirst_name(fnameFrom);
				fromUserModal.setLast_name(lNameFrom);

				thankedModel.setFromUser(fromUserModal);

				mostThankedModels.add(thankedModel);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mostThankedModels;
	}

	class ExcecuteLikePosts extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_like_post = WSConnector.LikePosts(params[2],
					params[3], params[4], params[5]);

			return result_like_post;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			// mostThankedModels = update_like_UI(jsonobjectString);
		}

	}

	public List<MostThankedModel> update_like_UI(String jsonobjectString) {
		// TODO Auto-generated method stub
		List<MostThankedModel> mostThankedModels = new ArrayList<MostThankedModel>();

		try {
			mJsonArray = new JSONArray(jsonobjectString);

			for (int i = 0; i < mJsonArray.length(); i++) {
				MostThankedModel thankedModel = new MostThankedModel();
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				post = jsonObject.getString("post");

				counter = jsonObject.getString("counter");

				thankedModel.setCounter(counter);
				thankedModel.setPost(post);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mostThankedModels;

	}

	class ExecuteCountry_Task extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_popular_countries = WSConnector.GetPopularCountries();
			return result_popular_countries;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setMessage("Loading..");
			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			list_of_countryModels = updateCountryModel(jsonobjectString);
			if (list_of_countryModels != null) {
				setupListForCities(list_of_countryModels);
			}

		}

		public List<CountryModel> updateCountryModel(String jsonobjectString) {
			// TODO Auto-generated method stub
			List<CountryModel> list_of_countryModels = new ArrayList<CountryModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					countryModel = new CountryModel();
					String country_code = jsonObject.getString("country_code");
					long country_longitude = jsonObject.getLong("longitude");
					long country_latitude = jsonObject.getLong("latitude");
					int country_messages = jsonObject.getInt("messages");

					countryModel.setCountry_code(country_code);
					countryModel.setCountry_latitude(country_latitude);
					countryModel.setCountry_longitude(country_longitude);
					countryModel.setCountry_messages(country_messages);
					list_of_countryModels.add(countryModel);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return list_of_countryModels;
		}

	}

	@SuppressWarnings("unchecked")
	public void setupListForCities(List<CountryModel> list_of_countryModels2) {
		// TODO Auto-generated method stub

		new ExecuteParsingCities().execute(list_of_countryModels2);

	}

	class ExecuteParsingCities extends
			AsyncTask<List<CountryModel>, Void, List<CountryModel>> {

		@Override
		protected List<CountryModel> doInBackground(
				List<CountryModel>... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < list_of_countryModels.size(); i++) {
				CountryModel countryModel = list_of_countryModels.get(i);
				String result = WSConnector.GetPopularCities(countryModel
						.getCountry_code());
				listcitiesModels = updateCitiesModel(result);
				countryModel.setCitiesModals(listcitiesModels);
			}
			return list_of_countryModels;
		}

		@Override
		protected void onPostExecute(List<CountryModel> jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);

			listAdapter = new ExpandableListAdapter(getActivity(),
					list_of_countryModels);
			// setting list adapter
			expandableListView.setAdapter(listAdapter);
		}

	}

	public List<CitiesModal> updateCitiesModel(String jsonobjectString) {
		// TODO Auto-generated method stub
		List<CitiesModal> listcitiesModels = new ArrayList<CitiesModal>();
		try {

			mJsonArray = new JSONArray(jsonobjectString);
			for (int i = 0; i < mJsonArray.length(); i++) {
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				CitiesModal citiesModal = new CitiesModal();
				String city_code = jsonObject.getString("city");
				long city_longitude = jsonObject.getLong("longitude");
				long city_latitude = jsonObject.getLong("latitude");
				int city_messages = jsonObject.getInt("messages");
				citiesModal.setCity_code(city_code);
				citiesModal.setCity_longitude(city_longitude);
				citiesModal.setCity_latitude(city_latitude);
				citiesModal.setCity_messages(city_messages);
				listcitiesModels.add(citiesModal);

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listcitiesModels;
	}

	// public class ExecuteParsingCities extends
	// AsyncTask<String, Integer, String> {
	//
	// String key;
	//
	// public ExecuteParsingCities(String key) {
	// super();
	// this.key = key;
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	// // TODO Auto-generated method stub
	// String result = WSConnector.GetPopularCities(key);
	// return result;
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// pDialog.setMessage("Loading..");
	// pDialog.show();
	// }
	//
	// @Override
	// protected void onPostExecute(String jsonobjectString) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(jsonobjectString);
	// pDialog.dismiss();
	// listcitiesModels = updateCitiesModel(jsonobjectString);
	// list_of_countryModels = updateList(key, listcitiesModels);
	//
	// }
	//
	// }
	//
	// public List<CitiesModal> updateCitiesModel(String jsonobjectString) {
	// // TODO Auto-generated method stub
	// List<CitiesModal> listcitiesModels = new ArrayList<CitiesModal>();
	// try {
	// mJsonArray = new JSONArray(jsonobjectString);
	// for (int i = 0; i < mJsonArray.length(); i++) {
	// JSONObject jsonObject = mJsonArray.getJSONObject(i);
	// CitiesModal citiesModal = new CitiesModal();
	// String city_code = jsonObject.getString("city");
	// long city_longitude = jsonObject.getLong("longitude");
	// long city_latitude = jsonObject.getLong("latitude");
	// int city_messages = jsonObject.getInt("messages");
	// citiesModal.setCity_code(city_code);
	// citiesModal.setCity_longitude(city_longitude);
	// citiesModal.setCity_latitude(city_latitude);
	// citiesModal.setCity_messages(city_messages);
	// listcitiesModels.add(citiesModal);
	//
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// }
	// return listcitiesModels;
	// }

	// public List<CountryModel> updateList(String key,
	// List<CitiesModal> listcitiesModels) {
	// // TODO Auto-generated method stub
	// for (int i = 0; i < list_of_countryModels.size(); i++) {
	// CountryModel countryModel = list_of_countryModels.get(i);
	// if (countryModel.getCitiesModals().equals(key)) {
	// list_of_countryModels.get(i).setCitiesModals(listcitiesModels);
	// break;
	// }
	// }
	// return list_of_countryModels;
	// }
	//
	public void sharedPrefernces() {
		// TODO Auto-generated method stub
		editor = sharedPreferences.edit();
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);
		// editor.clear();
		editor.commit();

	}

	public void clearPrefernces() {
		// TODO Auto-generated method stub
		editor = sharedPreferences.edit();

		editor.remove(token);
		editor.remove(uuid);
		editor.clear();
		editor.commit();
	}

}
