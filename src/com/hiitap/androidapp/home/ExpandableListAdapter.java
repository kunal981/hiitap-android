package com.hiitap.androidapp.home;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.hiitap.androidapp.R;
import com.hiitap.androidapp.modal.CitiesModal;
import com.hiitap.androidapp.modal.CountryModel;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
	TextView txt_Country,txt_messagecount,txt_cityname,txt_citymessagecount;
	private Context _context;
	private List<CountryModel> _list_of_countryModels;
	

	public ExpandableListAdapter(Context context,
			List<CountryModel> list_of_countryModels) {
		super();
		this._context = context;
		this._list_of_countryModels = list_of_countryModels;
		
	}

	
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return this._list_of_countryModels.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return this._list_of_countryModels.get(groupPosition).getCitiesModals().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return this._list_of_countryModels.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return this._list_of_countryModels.get(groupPosition).getCitiesModals().get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater lInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
			convertView = lInflater.inflate(R.layout.list_group, null);

		}
		CountryModel countryModel = (CountryModel)getGroup(groupPosition);
		 txt_Country = (TextView) convertView
				.findViewById(R.id.txt_ExpandableCountry);
		 txt_messagecount = (TextView)convertView.findViewById(R.id.txt_ExpandableCount);
		 if (countryModel.getCountry_code().equals("IN")) {
			 txt_Country.setText("India");
		}
		 else if (countryModel.getCountry_code().equals("AU")) {
			txt_Country.setText("Australia");
		}
		 else {
			txt_Country.setText(countryModel.getCountry_code());
		}
		
		 txt_messagecount.setText("TAPs:"+" "+String.valueOf(countryModel.getCountry_messages()));
		 
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.list_child, null);
		}
		CitiesModal citiesModal = (CitiesModal)getChild(groupPosition, childPosition);
		txt_cityname = (TextView)convertView.findViewById(R.id.txt_cityname);
		txt_citymessagecount = (TextView)convertView.findViewById(R.id.txt_citymessagecount);
		txt_cityname.setText(citiesModal.getCity_code()+" "+"-");
		txt_citymessagecount.setText("TAPs:"+" "+String.valueOf(citiesModal.getCity_messages()));
		
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
