package com.hiitap.androidapp.home;


import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hiitap.androidapp.R;

public class AboutActivity extends Activity {
	Button btn_back;
	ImageView imageView;
	TextView txt_about;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		btn_back = (Button)findViewById(R.id.back_button_about);
		imageView = (ImageView)findViewById(R.id.imgtap);
		btn_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		imageView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	
	txt_about = (TextView)findViewById(R.id.txt_about);
	
	txt_about.setMovementMethod(new ScrollingMovementMethod());
	}

}
