package com.hiitap.androidapp.home;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class Comment_Fragment extends Fragment {
	EditText edt_text;
	View rootView;
	InputMethodManager imm;
	ListView listView;
	String token, uuid_value, uuid, mStringcomment, person, message,
			comment_count;
	Adapter_Comment adapter_comment;
	TextView txt_name, txt_msg, mTextViewNoresult;
	ImageView img_profile, imageview_back;
	List<MostThankedModel> mostThankedModels;
	JSONArray mJsonArray;
	ProgressDialog pDialog;
	SharedPreferences sharedPreferences;
	Editor editor;
	Button btn_send;
	ImageButton imgbtn_back;
	List<String> list;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		pDialog = new ProgressDialog(getActivity());
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_comment, null);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		uuid_value = getArguments().getString("UUID_PASS");
		mStringcomment = getArguments().getString("COMMENT");
		person = getArguments().getString("PERSON_UUID");
		comment_count = getArguments().getString("COMMENT_COUNT");
		Log.e("person---------", "" + person);
		Log.e("uuid_value---------", "" + uuid_value);

		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		listView = (ListView) rootView.findViewById(R.id.listView_comment);
		edt_text = (EditText) rootView.findViewById(R.id.edt_comment);
		mTextViewNoresult = (TextView) rootView.findViewById(R.id.txtnoresult);
		btn_send = (Button) rootView.findViewById(R.id.btn_sendcomment);
		imageview_back = (ImageView) rootView.findViewById(R.id.imgtap);
		list = new ArrayList<String>();
		imgbtn_back = (ImageButton) rootView.findViewById(R.id.back_button);

		if (mStringcomment.equals("0")) {
			mTextViewNoresult.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			mostThankedModels = new ArrayList<MostThankedModel>();
			adapter_comment = new Adapter_Comment(getActivity(),
					mostThankedModels);
			listView.setAdapter(adapter_comment);
			list.add(message);

			adapter_comment.notifyDataSetChanged();
		} else {
			mTextViewNoresult.setVisibility(View.GONE);
			new ExecuteCommentTask().execute(AppConstant.BASE_URL,
					AppConstant.COMMENT_POSTS, uuid_value, token);

		}

		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				message = edt_text.getText().toString();
				Log.e("message---------", "" + message);
				if (edt_text.length() <= 1) {
					edt_text.setError("A message should contain atleast 1 character");
				} else {

					new ExecutePostComment().execute(AppConstant.BASE_URL,
							AppConstant.COMMENT, token, uuid_value, person,
							message);
					list.add(message);

					adapter_comment.notifyDataSetChanged();
				}

			}
		});

		imgbtn_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		imageview_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		return rootView;
	}

	public class Adapter_Comment extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public Adapter_Comment(Context context,
				List<MostThankedModel> mostThankedModels) {
			this.context = context;
			this.mostThankedModels = mostThankedModels;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mostThankedModels.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.fragment_comment_layout, null);

			}
			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);
			txt_name = (TextView) convertView.findViewById(R.id.txt_name);
			txt_name.setText(listDetailsModel.getName());
			txt_msg = (TextView) convertView.findViewById(R.id.txt_msg);
			txt_msg.setText(listDetailsModel.getMessage());
			img_profile = (ImageView) convertView
					.findViewById(R.id.img_profile);
			Picasso.with(getActivity())
					.load("https://www.hiitap.com/p/"
							+ listDetailsModel.getPerson()).into(img_profile);
			return convertView;
		}

	}

	class ExecuteCommentTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_comment = WSConnector.getCommentPosts(params[2],
					params[3]);
			return result_comment;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			mostThankedModels = updateUI_Comment(jsonobjectString);
			sharedPrefernces();
			adapter_comment = new Adapter_Comment(getActivity(),
					mostThankedModels);
			listView.setAdapter(adapter_comment);
		}

		private List<MostThankedModel> updateUI_Comment(String jsonobjectString) {
			mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					MostThankedModel thankedModel = new MostThankedModel();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					String uuid = jsonObject.getString("uuid");
					String person = jsonObject.getString("person");
					String name = jsonObject.getString("name");
					String message = jsonObject.getString("message");
					Log.e("jsonObject:", "" + jsonObject);

					thankedModel.setName(name);
					thankedModel.setPerson(person);
					thankedModel.setUUID(uuid);
					thankedModel.setMessage(message);

					mostThankedModels.add(thankedModel);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mostThankedModels;

		}

	}

	class ExecutePostComment extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_postComment = WSConnector.postComment(params[2],
					params[3], params[4], params[5]);
			return result_postComment;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			updateUI_comment(result);
		}

	}

	public void updateUI_comment(String result) {
		// TODO Auto-generated method stub
		new ExecuteCommentTask().execute(AppConstant.BASE_URL,
				AppConstant.COMMENT_POSTS, uuid_value, token);
		edt_text.setText("");
	}

	// public void onResume() {
	// TODO Auto-generated method stub

	// edt_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
	//
	// @Override
	// public void onFocusChange(View v, boolean hasFocus) {
	// // TODO Auto-generated method stub
	// edt_text.post(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// //
	// try {
	// imm = (InputMethodManager) getActivity()
	// .getSystemService(
	// Context.INPUT_METHOD_SERVICE);
	// imm.showSoftInput(edt_text,
	// InputMethodManager.SHOW_IMPLICIT);
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.getMessage();
	// }
	//
	// }
	// });
	// }
	// });
	// edt_text.requestFocus();
	//
	// super.onResume();
	//
	// }

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();

		Log.e("token: ", "" + token);

		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}
}
