package com.hiitap.androidapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hiitap.androidapp.webservices.WSConnector;

public class ForgotPassword extends Activity {
	ImageButton img_back;
	ImageView imageView;
	Button btn_reset_pass;
	EditText edt_email;
	String str_email;
	ProgressDialog pdialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_password);
		img_back = (ImageButton) findViewById(R.id.back_button_forgot);
		imageView = (ImageView) findViewById(R.id.img_tap);
		btn_reset_pass = (Button) findViewById(R.id.btn_resetpassword);
		edt_email = (EditText) findViewById(R.id.edt_email);
		pdialog = new ProgressDialog(ForgotPassword.this);
		img_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		imageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		btn_reset_pass.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				str_email = edt_email.getText().toString().trim();
				boolean receive = validation(str_email);
				if (receive) {
					new ExecuteTaskRequest().execute(AppConstant.BASE_URL,
							AppConstant.REQUEST_PASSWORD, str_email);
				}

			}
		});

	}

	
	protected boolean validation(String str_email) {
		// TODO Auto-generated method stub
		String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";

		if (str_email.matches("")) {
			edt_email.setError("Please enter your email address");
			return false;
		}
		if (!str_email.matches(emailPattern)) {
			edt_email.setError("Invalid email address");
			return false;
		}

		return true;
	}


	class ExecuteTaskRequest extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = WSConnector.resetpassword(params[2]);
			return result;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pdialog.setMessage("Loading...");
			pdialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pdialog.dismiss();
			showDialog();
		}

	}

	public void showDialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog
				.setTitle("Result")
				.setMessage(
						"Check your email to reset your password")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int ID) {
						// TODO Auto-generated method stub

						dialog.dismiss();

					}
				});
		AlertDialog dialog = alertDialog.create();
		dialog.show();
	}

}
