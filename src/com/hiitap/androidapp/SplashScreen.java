package com.hiitap.androidapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.hiitap.androidapp.webservices.WSConnector;

public class SplashScreen extends Activity {
	private static int SPLASH_TIME_OUT = 10;
	String username, password, token, response, uuid;
	SharedPreferences sharedPreferences;
	Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		sharedPreferences = getSharedPreferences(AppConstant.HII_TAP,
				Context.MODE_PRIVATE);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer.
			 */

			@Override
			public void run() {
				Intent isignin;
				// This method will be executed once the timer is over
				// Start your app main activity

				if (sharedPreferences != null
						&& sharedPreferences.contains(AppConstant.USERNAME)
						&& sharedPreferences.contains(AppConstant.PASSWORD)) {
					username = sharedPreferences.getString(
							AppConstant.USERNAME, "");
					password = sharedPreferences.getString(
							AppConstant.PASSWORD, "");

					new ExcecuteTask().execute(AppConstant.BASE_URL,
							AppConstant.LOGIN, username, password);
				}

				else {
					Intent i = new Intent(SplashScreen.this,
							WelcomePage_Activity.class);
					startActivityForResult(i, 1);
					finish();
				}

			}
		}, SPLASH_TIME_OUT);
	}

	class ExcecuteTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result = WSConnector.authenticate(params[2], params[3]);
			return result;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);

			updateUI(jsonobjectString);
			new ExecuteGetSession().execute(AppConstant.BASE_URL,
					AppConstant.GET_SESSION);
		}

	}

	public void updateUI(String jsonobjectString) {
		JSONArray jsonArrobject = null;
		try {
			jsonArrobject = new JSONArray(jsonobjectString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonobject = null;
		try {
			jsonobject = jsonArrobject.getJSONObject(0);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if (!jsonobject.has("token")) {

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						SplashScreen.this);
				alertDialog
						.setMessage(
								"Error while logging.Please check your internet connection")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
										onBackPressed();
										//
									}
								});
				AlertDialog dialog = alertDialog.create();
				dialog.show();

				return;

			}
			token = jsonobject.getString("token");
			Log.e("token:", "" + token);

			sharedPrefernces();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class ExecuteGetSession extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			return WSConnector.postSession(token);
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);

			getUpdatedSession(jsonobjectString);

		}

	}

	public void getUpdatedSession(String jsonobjectString) {
		// TODO Auto-generated method stub
		JSONArray jsonArrobject = null;
		try {
			jsonArrobject = new JSONArray(jsonobjectString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonobject = null;
		try {
			jsonobject = jsonArrobject.getJSONObject(0);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			uuid = jsonobject.getString("person");

			shared_Prefernces();
			Intent i = new Intent(SplashScreen.this, MainActivity.class);
			startActivity(i);
			finish();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();
		editor.putString(AppConstant.USERNAME, username);
		editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.commit();

	}

	public void shared_Prefernces() {
		editor = sharedPreferences.edit();

		editor.putString(AppConstant.USERNAME, username);
		editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();

	}
}
