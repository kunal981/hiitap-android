package com.hiitap.androidapp.tap;

import java.io.ByteArrayOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.BaseConvert;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.modal.UserModal;
import com.hiitap.androidapp.webservices.WSConnector;

public class TapViaEmail_fragment extends Fragment {

	View rootView;
	ImageButton img_back;
	ImageView image_message;
	EditText edt_message, edt_address;
	Button btn_send, btn_camera;
	String email, message, uuid, token, first_name, last_name,picturePath,base64path;
	ProgressDialog pDialog;
	SharedPreferences sharedPreferences;
	Editor editor;
	UserModal modal;
	JSONArray mJsonArray;
	private static int RESULT_LOAD_IMAGE = 1;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		pDialog = new ProgressDialog(getActivity());
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_tapviaemail, null);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		new ExecuteLineartabs().execute(AppConstant.BASE_URL,
				AppConstant.GET_PROFILE, uuid, token);
		img_back = (ImageButton) rootView.findViewById(R.id.img_back_button);
		edt_address = (EditText) rootView.findViewById(R.id.edt_tap_email);
		edt_message = (EditText) rootView.findViewById(R.id.edt_messsage);
		btn_camera = (Button) rootView.findViewById(R.id.btn_camera);
		btn_send = (Button) rootView.findViewById(R.id.btn_send_tap);
		image_message = (ImageView)rootView.findViewById(R.id.image_message);
		img_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		btn_camera.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});

		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				email = edt_address.getText().toString();
				message = edt_message.getText().toString();
				first_name = modal.getFirst_name();
				last_name = modal.getLast_name();
				if (email != null && email.length() <= 6) {
					edt_address.setError("Invalid email address");
				} else {
					 new ExecuteTask_sendEmail().execute(AppConstant.BASE_URL,
					 AppConstant.POST_MESSAGE,first_name,last_name, email,
					 message);
				}

			}
		});

		return rootView;
	}

	class ExecuteTask_sendEmail extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = WSConnector.postmessageEmail(params[2], params[3],
					params[4], params[5]);
			return result;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			sharedPrefernces();
			showDialog();

		}
	}

	class ExecuteLineartabs extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_profile = WSConnector
					.getProfile(params[2], params[3]);
			return result_profile;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			

			sharedPrefernces();
			modal = updateUI(jsonobjectString);

		}

	}

	public UserModal updateUI(String jsonobjectString) {
		// TODO Auto-generated method stub
		try {
			mJsonArray = new JSONArray(jsonobjectString);
			for (int i = 0; i < mJsonArray.length(); i++) {
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				modal = new UserModal();

				String uuid = jsonObject.getString("uuid");
				String firstname = jsonObject.getString("first_name");
				Log.e("firstname:", "" + firstname);
				String lastname = jsonObject.getString("last_name");
				Log.e("lastname:", "" + lastname);
				String sent = jsonObject.getString("sent");
				String received = jsonObject.getString("received");
				String followers = jsonObject.getString("followers");
				String following = jsonObject.getString("following");
				String follows = jsonObject.getString("follows");

				modal.setUUID(uuid);
				modal.setFirst_name(firstname);
				modal.setLast_name(lastname);
				modal.setSent(sent);
				modal.setFollowers(followers);
				modal.setReceived(received);
				modal.setFollowing(following);
				modal.setFollows(follows);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return modal;

	}

	public void showDialog() {
		
		// TODO Auto-generated method stub
		AlertDialog.Builder alertdialogPop = new AlertDialog.Builder(
				getActivity());
		alertdialogPop
				.setTitle("TAP SEND")
				.setMessage("Mail Successfully Sent To Tap via Email")
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
								getActivity().onBackPressed();

							}
						});
		AlertDialog dialog = alertdialogPop.create();
		dialog.show();
	}
		
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			picturePath = cursor.getString(columnIndex);
			
			cursor.close();
			

			try {
				// hide video preview

				// bimatp factory
				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for
				// larger
				// images
				options.inSampleSize = 1;

				Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
				Bitmap mBitmap=Bitmap.createScaledBitmap(bitmap, 100, 100, false);

				// Bitmap mBitmap_send = StringToBitMap(picturePath);
				// Log.e("mBitmap_send: ", ""+mBitmap_send);
				image_message.setImageBitmap(mBitmap);
				base64path = convert_image(mBitmap);
				Log.e("base64path: ", "" + base64path);

			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			// base64path=setPic(picturePath);

		}
	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Log.e("STREAM", "" + stream);
		// Log.e("BITMAP: ", "" + bitmap);
		bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream); // compress to
		// which
		// format
		// you want.

		byte[] byte_arr = stream.toByteArray();
		// long lenthbmp = byte_arr.length;
		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	public void sharedPrefernces() {
		// TODO Auto-generated method stub
		editor = sharedPreferences.edit();
		// Log.e("first_name========", ""+first_name);
		// Log.e("last_name========", ""+last_name);

		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);
		// editor.putString(AppConstant.FIRST_NAME, first_name);
		// editor.putString(AppConstant.LAST_NAME, last_name);

		editor.commit();
	}

}
