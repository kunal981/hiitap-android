package com.hiitap.androidapp.tap;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.BaseConvert;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class TapFragmentSpinner extends Fragment implements
		OnItemSelectedListener {
	Button btn_camera, btn_send;
	String picturePath, token, to_user, message, action, image, image_type,
			uuid, firtsname, lastname;
	Spinner spinner;
	InputMethodManager imm;

	ImageView imgView, imageProfile;
	ImageButton img_back;
	EditText edt_text;
	String base64path;
	SharedPreferences sharedPreferences;
	Editor editor;
	TextView txt_title;
	ProgressDialog pDialog;
	private static int RESULT_LOAD_IMAGE = 1;

	String[] tap = new String[] { "Thanks", "Encouragment", "Support",
			"Prayer", "Congratulations",

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_tap_spinner,
				container, false);

		if (getArguments() != null) {
			Bundle bundle = this.getArguments();
			to_user = bundle.getString("UUID_PASSED");
			firtsname = bundle.getString("FIRSTNAME");
			lastname = bundle.getString("LASTNAME");
		}

		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		edt_text = (EditText) rootView.findViewById(R.id.editText1);
		btn_camera = (Button) rootView.findViewById(R.id.btn_camera);

		spinner = (Spinner) rootView.findViewById(R.id.spinner1);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line, tap);

		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		btn_send = (Button) rootView.findViewById(R.id.btn_send);
		imgView = (ImageView) rootView.findViewById(R.id.imageView2);
		img_back = (ImageButton) rootView.findViewById(R.id.back_button);
		txt_title = (TextView) rootView.findViewById(R.id.txt_title);
		txt_title.setText("Tap" + " " + firtsname + " " + lastname);
		imageProfile = (ImageView) rootView.findViewById(R.id.img_profilepic);
		Picasso.with(getActivity()).load("https://www.hiitap.com/p/" + to_user)
				.into(imageProfile);
		img_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				message = edt_text.getText().toString();
				action = spinner.getSelectedItem().toString();
				Log.e("base64path===", ""+base64path);
//				boolean receive = validateSuccess(message);
//				if (receive) {
//						
//				}
//				else {
//					
//				}
				if (edt_text.length()<=15) {
					edt_text.setError("A message should contains atleast 15 characters");
				}
				else {
					new ExcecuteTask().execute(AppConstant.BASE_URL,
						AppConstant.POST_MESSAGE, token, to_user, action,
							message, base64path, "png");
				}
				
			}


	});

		return rootView;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		try {
			((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

			((TextView) parent.getChildAt(0)).setTextSize(15);

		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

		edt_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				edt_text.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						// imm = (InputMethodManager) getActivity()
						// .getSystemService(Context.INPUT_METHOD_SERVICE);
						// imm.showSoftInput(edt_text,
						// InputMethodManager.SHOW_IMPLICIT);
						try {
							imm = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.showSoftInput(edt_text,
									InputMethodManager.SHOW_IMPLICIT);
						} catch (Exception e) {
							// TODO: handle exception
							e.getMessage();
						}

					}
				});
			}
		});
		edt_text.requestFocus();

		btn_camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});

		super.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			picturePath = cursor.getString(columnIndex);
			
			cursor.close();
			

			try {
				// hide video preview

				// bimatp factory
				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for
				// larger
				// images
				options.inSampleSize = 1;

				Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
				Bitmap mBitmap=Bitmap.createScaledBitmap(bitmap, 100, 100, false);

				// Bitmap mBitmap_send = StringToBitMap(picturePath);
				// Log.e("mBitmap_send: ", ""+mBitmap_send);
				imgView.setImageBitmap(mBitmap);
				base64path = convert_image(mBitmap);
				Log.e("base64path: ", "" + base64path);

			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			// base64path=setPic(picturePath);

		}
	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Log.e("STREAM", "" + stream);
		// Log.e("BITMAP: ", "" + bitmap);
		bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream); // compress to
		// which
		// format
		// you want.

		byte[] byte_arr = stream.toByteArray();
		// long lenthbmp = byte_arr.length;
		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	// private String setPic(String mStringPath) {
	// // TODO Auto-generated method stub
	// int targetWidth = imgView.getWidth();
	// int targetHeight = imgView.getHeight();
	//
	// BitmapFactory.Options options = new BitmapFactory.Options();
	// options.inJustDecodeBounds = true;
	// BitmapFactory.decodeFile(picturePath, options);
	//
	// int photoWidth = options.outWidth;
	// int photoHeight = options.outHeight;
	//
	// int scaleFactor = Math.min(photoWidth / targetWidth, photoHeight
	// / targetHeight);
	//
	// options.inJustDecodeBounds = false;
	// options.inSampleSize = scaleFactor;
	// options.inPurgeable = true;
	// Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
	// imgView.setImageBitmap(bitmap);
	// ByteArrayOutputStream stream = new ByteArrayOutputStream();
	// Log.e("STREAM", "" + stream);
	// Log.e("BITMAP: ", "" + bitmap);
	// bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream); // compress to
	// // which
	// // format
	// // you want.
	// byte[] byte_arr = stream.toByteArray();
	// String mStringBase64 = BaseConvert.encodeBytes(byte_arr);
	// return mStringBase64;
	// }

	class ExcecuteTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_postMessage = WSConnector.postMessage(params[2],
					params[3], params[4], params[5], params[6], params[7]);
			return result_postMessage;
		}
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setMessage("Loading...");

			pDialog.show();
		}
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			showDialog();
		}
		private void showDialog() {
			// TODO Auto-generated method stub
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(
					getActivity());
			alertDialog
			.setTitle("TAP SEND")
			.setMessage("Successfully Delivered To "+" "+firtsname+" "+lastname)
			.setCancelable(false)
			.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int ID) {
							// TODO Auto-generated method stub
							
							dialog.dismiss();
							getActivity().onBackPressed();
						}
					});
	AlertDialog dialog = alertDialog.create();
	dialog.show();
		}

	}

	public void sharedPrefernces() {
		// TODO Auto-generated method stub
		editor = sharedPreferences.edit();
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);
		
		editor.commit();

	}

}
