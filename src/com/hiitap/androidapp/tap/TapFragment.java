package com.hiitap.androidapp.tap;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class TapFragment extends Fragment implements OnEditorActionListener {

	// Array of strings storing country names
	ListView listView;
	Button btnCancel;
	EditText edttxt;
	InputMethodManager imm;
	String uuid, token, person, query;
	SharedPreferences sharedPreferences;
	Editor editor;
	TextView txt_fname, txt_name, txt_sendtap;
	ImageView img_profile;
	JSONArray mJsonArray;
	Boolean check = false;
	Adapter_Tap adapter;
	ArrayList<String> mArrayList;
	ProgressDialog pDialog;
	View rootView;
	List<MostThankedModel> mostThankedModels;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_tap, null);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		Log.e("uuid: ", "" + uuid);
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		mArrayList = new ArrayList<String>();
		listView = (ListView) rootView.findViewById(R.id.listView1search);
		btnCancel = (Button) rootView.findViewById(R.id.btn_cancel);
		edttxt = (EditText) rootView.findViewById(R.id.editText1);
		txt_sendtap = (TextView) rootView.findViewById(R.id.txt_tapsend);

		new ExecuteTaskTap().execute(AppConstant.BASE_URL,
				AppConstant.GET_FOLLOWING, uuid, token);

		edttxt = (EditText) rootView.findViewById(R.id.editText1);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				final MostThankedModel listDetailsModel = mostThankedModels
						.get(position);
				String uuid = listDetailsModel.getUUID();
				String firstname = listDetailsModel.getFirst_name();
				String lastname = listDetailsModel.getLast_name();

				Log.e("uuid Tap Fragment-----", "" + uuid);
				TapFragmentSpinner fragment = new TapFragmentSpinner();
				Bundle savedInstanceState = new Bundle();
				savedInstanceState.putString("UUID_PASSED", uuid);
				savedInstanceState.putString("FIRSTNAME", firstname);
				savedInstanceState.putString("LASTNAME", lastname);
				fragment.setArguments(savedInstanceState);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);
			}

		});
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				edttxt.setText("");
			}
		});
		txt_sendtap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TapViaEmail_fragment email_fragment = new TapViaEmail_fragment();
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						email_fragment, true);
			}
		});

		return rootView;
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.editText1:
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				// Log.i("Splash", "editor go button cllicked");
				// Toast.makeText(getActivity(), "hello",
				// Toast.LENGTH_SHORT).show();
				// onClickLoginPin(null);
				// startHomeActivity();
				check = true;
				query = edttxt.getText().toString();
				new ExecuteSearchBarTask().execute(AppConstant.BASE_URL,
						AppConstant.SEARCH, uuid, token, query);
			}
			break;

		default:
			break;
		}

		return true;
	}

	public class Adapter_Tap extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public Adapter_Tap(Context context,
				List<MostThankedModel> mostThankedModels) {
			this.context = context;
			this.mostThankedModels = mostThankedModels;
			this.inflater = inflater;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mArrayList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.listview_layout1, null);

			}
			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);
			txt_fname = (TextView) convertView.findViewById(R.id.txt_name);

			img_profile = (ImageView) convertView.findViewById(R.id.flag);
			if (check) {
				txt_fname.setText(listDetailsModel.getTitle());
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/"
								+ listDetailsModel.getUUID()).into(img_profile);
			} else {
				txt_fname.setText(listDetailsModel.getFirst_name() + " "
						+ listDetailsModel.getLast_name());
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/"
								+ listDetailsModel.getUUID()).into(img_profile);
			}

			return convertView;

		}

	}

	class ExecuteTaskTap extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_tap = WSConnector.getFollowing(params[2], params[3]);
			return result_tap;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			// mArrayList.clear();
			check = false;

			mostThankedModels = updateUI_Search(jsonobjectString);
			sharedPreferences();
			adapter = new Adapter_Tap(getActivity(), mostThankedModels);
			listView.setAdapter(adapter);

		}

		private List<MostThankedModel> updateUI_Search(String jsonobjectString) {
			// TODO Auto-generated method stub
			mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					MostThankedModel thankedModel = new MostThankedModel();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					String uuid = jsonObject.getString("uuid");
					String firstname = jsonObject.getString("first_name");
					String lastname = jsonObject.getString("last_name");
					Log.e("jsonObject:", "" + jsonObject);
					mArrayList.add(uuid);
					thankedModel.setUUID(uuid);
					thankedModel.setFirst_name(firstname);
					thankedModel.setLast_name(lastname);

					mostThankedModels.add(thankedModel);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mostThankedModels;

		}

	}

	class ExecuteSearchBarTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = WSConnector.getSearch(params[2], params[3],
					params[4]);
			return result;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			mArrayList.clear();

			Log.e("jsonobjectString: ", "" + jsonobjectString);
			List<MostThankedModel> mostThankedModels = updateUI_EdittextSearch(jsonobjectString);
			Log.e("mostThankedModels: ", "" + mostThankedModels);
			adapter = new Adapter_Tap(getActivity(), mostThankedModels);
			listView.setAdapter(adapter);
			// adapter.notifyDataSetChanged();

		}

		private List<MostThankedModel> updateUI_EdittextSearch(
				String jsonobjectString) {
			mArrayList.clear();
			// TODO Auto-generated method stub
			List<MostThankedModel> mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				Log.e("jsonobjectString search", "" + jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					MostThankedModel thankedModel = new MostThankedModel();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					String uuid = jsonObject.getString("uuid");
					String type = jsonObject.getString("type");
					String title = jsonObject.getString("title");
					Log.e("jsonObject:", "" + jsonObject);
					mArrayList.add(uuid);
					thankedModel.setUUID(uuid);
					;
					thankedModel.setTitle(title);
					thankedModel.setType(type);

					mostThankedModels.add(thankedModel);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mostThankedModels;

		}
	}

	public void sharedPreferences() {
		editor = sharedPreferences.edit();

		Log.e("token: ", "" + token);
		Log.e("uuid: ", "" + uuid);
		// editor.putString(AppConstant.USERNAME, username);
		// editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}
}
