package com.hiitap.androidapp.modal;

public class Post_FeedbackModal {
	private String UUID;
	private String status;
	private String person_uuid;
	private String subject;
	private String details;
	private String created;
	
	
	public String getUUID() {
		return UUID;
	}
	public void setUUID(String uUID) {
		UUID = uUID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPerson_uuid() {
		return person_uuid;
	}
	public void setPerson_uuid(String person_uuid) {
		this.person_uuid = person_uuid;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
	

}
