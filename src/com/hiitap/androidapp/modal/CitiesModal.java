package com.hiitap.androidapp.modal;

public class CitiesModal {
	private String city_code;
	private long city_longitude;
	private long city_latitude;
	private int city_messages;

	public long getCity_longitude() {
		return city_longitude;
	}

	public void setCity_longitude(long city_longitude) {
		this.city_longitude = city_longitude;
	}

	public long getCity_latitude() {
		return city_latitude;
	}

	public void setCity_latitude(long city_latitude) {
		this.city_latitude = city_latitude;
	}

	public int getCity_messages() {
		return city_messages;
	}

	public void setCity_messages(int city_messages) {
		this.city_messages = city_messages;
	}

	public String getCity_code() {
		return city_code;
	}

	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}

}
