package com.hiitap.androidapp.modal;

public class ListDetailsModel {
	String Title;
	String Comment;
	String Date;
	String Image;
	
	
	

	public ListDetailsModel() {
		
	}

	public ListDetailsModel(String title, String comment, String date,
			String image) {
		super();
		Title = title;
		Comment = comment;
		Date = date;
		Image = image;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String decs) {
		this.Title = decs;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comments) {
		this.Comment = comments;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		this.Date = date;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		this.Image = image;
	}

}
