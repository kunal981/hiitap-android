package com.hiitap.androidapp.modal;

import java.util.List;

public class CountryModel {
	private String country_code;
	private long country_longitude;
	private long country_latitude;
	private int country_messages;
	List<CitiesModal> citiesModals;

	public List<CitiesModal> getCitiesModals() {
		return citiesModals;
	}

	public void setCitiesModals(List<CitiesModal> citiesModals) {
		this.citiesModals = citiesModals;
	}

	public String getCountry_code() {
		return country_code;
	}

	public long getCountry_longitude() {
		return country_longitude;
	}

	public void setCountry_longitude(long country_longitude) {
		this.country_longitude = country_longitude;
	}

	public long getCountry_latitude() {
		return country_latitude;
	}

	public void setCountry_latitude(long country_latitude) {
		this.country_latitude = country_latitude;
	}

	public int getCountry_messages() {
		return country_messages;
	}

	public void setCountry_messages(int country_messages) {
		this.country_messages = country_messages;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	

	

}
