package com.hiitap.androidapp.recent;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.home.Comment_Fragment;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.modal.UserModal;
import com.hiitap.androidapp.profile.LikeDetails_fragment;
import com.hiitap.androidapp.util.Utility;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class RecentFragment extends Fragment {
	
	String response, uuid, token, post, counter;
	ListView listView;
	ListAdapter1 adapter1;

	boolean isClicked = false;
	JSONObject jsonObject;
	JSONArray mJsonArray;
	ProgressDialog pDialog;
	View rootView;
	List<MostThankedModel> mostThankedModels;
	SharedPreferences sharedPreferences;
	Editor editor;
	ViewHolder holder;
	Button btn_refresh;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// View rootView = inflater.inflate(R.layout.fragment_recent, container,
		// false);
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_recent, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		Log.e("uuid: ", "" + uuid);
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		listView = (ListView) rootView.findViewById(R.id.llvrecent);

		getData();

		btn_refresh = (Button) rootView.findViewById(R.id.btn_recentRefesh);

		btn_refresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getData();
				 Toast.makeText(getActivity(), "Refreshing..",
				 Toast.LENGTH_LONG)
				 .show();
			}
		});

		return rootView;
	}

	private void getData() {
		// TODO Auto-generated method stub
		new RecentPost().execute(AppConstant.BASE_URL, AppConstant.GET_PROFILE,
				uuid, token);
	}

	class ListAdapter1 extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public ListAdapter1(Context context,
				List<MostThankedModel> mostThankedModels) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.mostThankedModels = mostThankedModels;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mostThankedModels.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.activity_most_liked,
						null);
				holder = new ViewHolder();
				holder.txt_large = (TextView) convertView
						.findViewById(R.id.txtLArgeText);
				holder.txt_comment = (TextView) convertView
						.findViewById(R.id.txt_comments);
				holder.txt_date = (TextView) convertView
						.findViewById(R.id.txt_time);
				holder.img_flag = (ImageView) convertView
						.findViewById(R.id.flag);
				holder.txt_date = (TextView) convertView
						.findViewById(R.id.txt_time);
				holder.txt_commentCount = (TextView) convertView
						.findViewById(R.id.txtcomments_count);
				holder.txt_likeCount = (TextView) convertView
						.findViewById(R.id.likes_txt);

				holder.btn_comment = (Button) convertView
						.findViewById(R.id.btn_commentimg);
				holder.btn_heart = (Button) convertView
						.findViewById(R.id.btn_hearticon);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);
			if (listDetailsModel.getAction().equals("follow")) {
				holder.txt_large.setText(listDetailsModel.getMessage());
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/"
								+ listDetailsModel.getToUser().getUUID())
						.into(holder.img_flag);
				String time = Utility.convertTime(listDetailsModel.getTime());
				holder.txt_date.setText(time);
				holder.txt_comment.setVisibility(View.GONE);
				holder.txt_commentCount.setVisibility(View.GONE);
				holder.btn_comment.setVisibility(View.GONE);
				holder.btn_heart.setVisibility(View.GONE);
				holder.txt_likeCount.setVisibility(View.GONE);

			} else {
				holder.txt_large.setText(listDetailsModel.getFromUser()
						.getFirst_name()
						+ " "
						+ listDetailsModel.getFromUser().getLast_name()
						+ " "
						+ "sent"
						+ " "
						+ listDetailsModel.getAction()
						+ " "
						+ "to"
						+ " "
						+ listDetailsModel.getToUser().getFirst_name()
						+ " "
						+ listDetailsModel.getToUser().getLast_name());

				holder.txt_comment.setText(listDetailsModel.getMessage());

				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/"
								+ listDetailsModel.getToUser().getUUID())
						.into(holder.img_flag);
				String time = Utility.convertTime(listDetailsModel.getTime());
				holder.txt_date.setText(time);

				holder.txt_commentCount.setText("Comments " + " "
						+ listDetailsModel.getComment_count());

				holder.txt_likeCount.setText("Like " + " "
						+ listDetailsModel.getLike_count());
				holder.txt_likeCount
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								String firstUUID = listDetailsModel.getUUID();
								Log.e("firstUUID......=====", "" + firstUUID);
								LikeDetails_fragment details_fragment = new LikeDetails_fragment();
								Bundle savedInstanceState = new Bundle();
								savedInstanceState.putString("UUID_PASS",
										firstUUID);
								details_fragment
										.setArguments(savedInstanceState);
								((BaseContainerFragment) getParentFragment())
										.replaceFragment(details_fragment, true);
							}
						});

				holder.txt_commentCount
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								String getUuid = listDetailsModel.getUUID();
								String mString_comment = listDetailsModel
										.getComment_count();
								String person_uuid = listDetailsModel
										.getToUser().getUUID();
								Comment_Fragment comment_Fragment = new Comment_Fragment();
								Bundle savedInstanceState = new Bundle();
								savedInstanceState.putString("UUID_PASS",
										getUuid);
								savedInstanceState.putString("COMMENT",
										mString_comment);
								savedInstanceState.putString("PERSON_UUID",
										person_uuid);
								comment_Fragment
										.setArguments(savedInstanceState);
								((BaseContainerFragment) getParentFragment())
										.replaceFragment(comment_Fragment, true);
								
							}
						});

				holder.btn_comment
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								String getUuid = listDetailsModel.getUUID();
								String mString_comment = listDetailsModel
										.getComment_count();
								String person_uuid = listDetailsModel
										.getToUser().getUUID();
								Comment_Fragment comment_Fragment = new Comment_Fragment();
								Bundle savedInstanceState = new Bundle();
								savedInstanceState.putString("UUID_PASS",
										getUuid);
								savedInstanceState.putString("COMMENT",
										mString_comment);
								savedInstanceState.putString("PERSON_UUID",
										person_uuid);
								comment_Fragment
										.setArguments(savedInstanceState);
								((BaseContainerFragment) getParentFragment())
										.replaceFragment(comment_Fragment, true);
							}
						});

				if (listDetailsModel.getLiked().matches("true")) {
					holder.btn_heart.setBackgroundResource(R.drawable.heart);

				} else {
					holder.btn_heart.setBackgroundResource(R.drawable.heart1);
				}

				holder.btn_heart.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String firstUUID = listDetailsModel.getUUID();
						String postTo = listDetailsModel.getToUser().getUUID();
						String like = listDetailsModel.getLiked();

						String txt_ = listDetailsModel.getLike_count();
						String like_status = "false";
						if (like.equals("false")) {
							like_status = "true";

							mostThankedModels.get(position).setLiked(
									like_status);

							int myNum = Integer.parseInt(txt_) + 1;
							// holder.txtlike.setText("Like"+" "+myNum);
							mostThankedModels.get(position).setLike_count(
									String.valueOf(myNum));

							notifyDataSetChanged();
							new ExcecuteLikePosts().execute(
									AppConstant.BASE_URL,
									AppConstant.LIKE_POSTS, token, firstUUID,
									postTo, like_status);

						} else if (like.equals("true")) {

							like_status = "false";
							// v.setBackgroundResource(R.drawable.heart1);
							mostThankedModels.get(position).setLiked(
									like_status);

							int myNum = Integer.parseInt(txt_) - 1;
							// holder.txtlike.setText("Like"+" "+myNum);
							mostThankedModels.get(position).setLike_count(
									String.valueOf(myNum));

							// holder.txtlike.setText("Like"+" "+myNum1);
							notifyDataSetChanged();
							new ExcecuteLikePosts().execute(
									AppConstant.BASE_URL,
									AppConstant.LIKE_POSTS, token, firstUUID,
									postTo, like_status);
						}

					}
				});
			}
			return convertView;
		}
	}

	static class ViewHolder {
		ImageView imageView, img_flag;
		Button btn_heart, btn_comment;
		TextView txtheart_icon, txt_comment, txtlike, txtcomment, txt_cur,
				txt_con, txt_large, txt_date, txt_likeCount, txt_commentCount;
	}

	class RecentPost extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result =  WSConnector.getRecentFeeds(params[2], params[3]);
			return result;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			List<MostThankedModel> list = updateUI(jsonobjectString);
			sharedPrefernces();
			adapter1 = new ListAdapter1(getActivity(), mostThankedModels);
			listView.setAdapter(adapter1);
			pDialog.dismiss();
		}
	}

	public List<MostThankedModel> updateUI(String jsonobjectString) {
		// TODO Auto-generated method stub
		mostThankedModels = new ArrayList<MostThankedModel>();
		try {
			mJsonArray = new JSONArray(jsonobjectString);
			Log.e("mJsonArray: ", "" + mJsonArray.length());
			Log.e("mJsonArray: ", "" + mJsonArray);
			for (int i = 0; i < mJsonArray.length(); i++) {
				MostThankedModel thankedModel = new MostThankedModel();
				UserModal userModal = new UserModal();
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				String uuid = jsonObject.getString("uuid");
				String action = jsonObject.getString("action");
				String message = jsonObject.getString("message");
				String like_count = jsonObject.getString("like_count");
				String comment_count = jsonObject.getString("comment_count");
				String time = jsonObject.getString("time");
				String liked = jsonObject.getString("liked");
				// Todo get remaining items

				thankedModel.setUUID(uuid);
				thankedModel.setAction(action);
				thankedModel.setMessage(message);
				thankedModel.setLike_count(like_count);
				thankedModel.setComment_count(comment_count);
				thankedModel.setTime(time);
				thankedModel.setLiked(liked);

				UserModal toUserModel = new UserModal();
				JSONObject jsonInnerToOject = jsonObject.getJSONObject("to");
				String uuidtoInner = jsonInnerToOject.getString("uuid");
				String fNameTo = jsonInnerToOject.getString("first_name");
				String lNameTo = jsonInnerToOject.getString("last_name");
				// Todo get remaining items
				toUserModel.setUUID(uuidtoInner);
				toUserModel.setFirst_name(fNameTo);
				toUserModel.setLast_name(lNameTo);

				thankedModel.setToUser(toUserModel);

				// Todo set usermodel for from
				UserModal fromUserModal = new UserModal();
				JSONObject jsonfromInner = jsonObject.getJSONObject("from");
				String uuidFrom = jsonfromInner.getString("uuid");
				String fnameFrom = jsonfromInner.getString("first_name");
				String lNameFrom = jsonfromInner.getString("last_name");
				fromUserModal.setUUID(uuidFrom);
				fromUserModal.setFirst_name(fnameFrom);
				fromUserModal.setLast_name(lNameFrom);

				thankedModel.setFromUser(fromUserModal);

				mostThankedModels.add(thankedModel);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mostThankedModels;
	}

	class ExcecuteLikePosts extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_like_post = WSConnector.LikePosts(params[2],
					params[3], params[4], params[5]);

			return result_like_post;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			// mostThankedModels = update_like_UI(jsonobjectString);
		}

	}

	public List<MostThankedModel> update_like_UI(String jsonobjectString) {
		// TODO Auto-generated method stub
		List<MostThankedModel> mostThankedModels = new ArrayList<MostThankedModel>();

		try {
			mJsonArray = new JSONArray(jsonobjectString);
			Log.e("", "" + jsonobjectString);
			for (int i = 0; i < mJsonArray.length(); i++) {
				MostThankedModel thankedModel = new MostThankedModel();
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				post = jsonObject.getString("post");
				Log.e("post===========", "" + post);
				counter = jsonObject.getString("counter");
				Log.e("counter===========", "" + counter);
				thankedModel.setCounter(counter);
				thankedModel.setPost(post);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mostThankedModels;

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();

		Log.e("token: ", "" + token);
		Log.e("uuid: ", "" + uuid);
		// editor.putString(AppConstant.USERNAME, username);
		// editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}
}
