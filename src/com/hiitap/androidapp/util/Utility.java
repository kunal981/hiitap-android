package com.hiitap.androidapp.util;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class Utility {

	public static String convertTime(String timeInMills) {
		// String time_string =
		// time.split("\\(")[1].split("\\+")[0];
		// Date date = new Date(Long.parseLong(time_string));
		// SimpleDateFormat dateFormat = new
		// SimpleDateFormat("DD\\MM\\YYYY");

		long millisecond = Long.parseLong(timeInMills);
//		String dateString = DateFormat.format("MMM dd hh:mm",
//				new Date(millisecond)).toString();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd hh:mm");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String reString = dateFormat.format(millisecond);
		String result = reString.substring(0,7)+":"+reString.substring(8,12);
		
		return result;
	}
}
