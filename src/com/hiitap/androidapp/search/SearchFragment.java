package com.hiitap.androidapp.search;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.profile.ProfileFragment;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class SearchFragment extends Fragment {
	EditText edt_text;
	ListView listView;
	InputMethodManager imm;
	Adapter_Search adapter;

	String uuid, token, person, query;
	SharedPreferences sharedPreferences;
	Editor editor;

	JSONArray mJsonArray;
	Boolean check = false;
	ArrayList<String> mArrayList;
	ProgressDialog pDialog;
	View rootView;
	List<MostThankedModel> mostThankedModels;
	ViewHolder holder;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_search, null);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		// value = getArguments().getString("UUID_PASS");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		mArrayList = new ArrayList<String>();

		
		listView = (ListView) rootView.findViewById(R.id.list_view_search);

		new ExecuteSearchTask().execute(AppConstant.BASE_URL,
				AppConstant.GET_FOLLOWING, uuid, token);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final MostThankedModel listDetailsModel = mostThankedModels
						.get(position);
				String mString = listDetailsModel.getUUID();
				ProfileFragment fragment = new ProfileFragment();
				Bundle savedInstanceState = new Bundle();
				savedInstanceState.putString("UUID_PASS", mString);
				fragment.setArguments(savedInstanceState);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);

			}

		});
		edt_text = (EditText) rootView.findViewById(R.id.edt_search);
		edt_text.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.edt_search:
					if (actionId == EditorInfo.IME_ACTION_DONE) {

						check = true;

						query = edt_text.getText().toString();
						new ExecuteSearchBarTask().execute(
								AppConstant.BASE_URL, AppConstant.SEARCH, uuid,
								token, query);
					}
					break;

				default:
					break;
				}
				return true;
			}
		});

		return rootView;
	}

	public class Adapter_Search extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public Adapter_Search(Context context,
				List<MostThankedModel> mostThankedModels) {
			this.context = context;
			this.mostThankedModels = mostThankedModels;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mArrayList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.listview_layout1, null);
				holder = new ViewHolder();
				holder.txt_fname = (TextView) convertView
						.findViewById(R.id.txt_name);
				holder.img_profile = (ImageView) convertView
						.findViewById(R.id.flag);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);

			if (check) {
				holder.txt_fname.setText(listDetailsModel.getTitle());
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/"
								+ listDetailsModel.getUUID())
						.into(holder.img_profile);
			} else {
				holder.txt_fname.setText(listDetailsModel.getFirst_name() + " "
						+ listDetailsModel.getLast_name());
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/"
								+ listDetailsModel.getUUID())
						.into(holder.img_profile);
			}

			return convertView;

		}

	}

	static class ViewHolder {
		TextView txt_fname;
		ImageView img_profile;
	}

	class ExecuteSearchTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_following = WSConnector.getFollowing(params[2],
					params[3]);
			return result_following;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			mArrayList.clear();
			check = false;
			
			mostThankedModels = updateUI_Search(jsonobjectString);
			sharedPreferences();
			adapter = new Adapter_Search(getActivity(), mostThankedModels);
			listView.setAdapter(adapter);
			adapter.notifyDataSetChanged();

		}

		private List<MostThankedModel> updateUI_Search(String jsonobjectString) {
			// TODO Auto-generated method stub
			mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					MostThankedModel thankedModel = new MostThankedModel();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					String uuid = jsonObject.getString("uuid");
					String firstname = jsonObject.getString("first_name");
					String lastname = jsonObject.getString("last_name");
					Log.e("jsonObject:", "" + jsonObject);
					mArrayList.add(uuid);
					thankedModel.setUUID(uuid);
					thankedModel.setFirst_name(firstname);
					thankedModel.setLast_name(lastname);

					mostThankedModels.add(thankedModel);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mostThankedModels;

		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub

		edt_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				edt_text.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						//
						try {
							imm = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.showSoftInput(edt_text,
									InputMethodManager.SHOW_IMPLICIT);
						} catch (Exception e) {
							// TODO: handle exception
							e.getMessage();
						}

					}
				});
			}
		});
		edt_text.requestFocus();

		super.onResume();

	}

	class ExecuteSearchBarTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_searchBar = WSConnector.getSearch(params[2],
					params[3], params[4]);
			return result_searchBar;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			mArrayList.clear();

			Log.e("jsonobjectString: ", "" + jsonobjectString);
			mostThankedModels = updateUI_EdittextSearch(jsonobjectString);
			Log.e("mostThankedModels: ", "" + mostThankedModels);
			adapter = new Adapter_Search(getActivity(), mostThankedModels);
			listView.setAdapter(adapter);
			adapter.notifyDataSetChanged();

		}

		private List<MostThankedModel> updateUI_EdittextSearch(
				String jsonobjectString) {
			// mArrayList.clear();
			// TODO Auto-generated method stub
			mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				Log.e("jsonobjectString search", "" + jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					MostThankedModel thankedModel = new MostThankedModel();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					String uuid = jsonObject.getString("uuid");
					String type = jsonObject.getString("type");
					String title = jsonObject.getString("title");
					Log.e("jsonObject:", "" + jsonObject);
					mArrayList.add(uuid);
					thankedModel.setUUID(uuid);
					thankedModel.setTitle(title);
					thankedModel.setType(type);

					mostThankedModels.add(thankedModel);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mostThankedModels;

		}
	}

	public void sharedPreferences() {
		editor = sharedPreferences.edit();
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}

}
