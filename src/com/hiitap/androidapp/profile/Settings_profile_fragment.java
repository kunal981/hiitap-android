package com.hiitap.androidapp.profile;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;

public class Settings_profile_fragment extends Fragment {
	View rootView;
	ImageButton img_back;
	
	ListView listView;
	TextView txt_item;
	Adapter_Settings adapter;
	ImageView imageView,imageview_back;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_profile_settings, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		
		img_back=(ImageButton)rootView.findViewById(R.id.back_button);
		img_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		imageview_back = (ImageView)rootView.findViewById(R.id.imgtap);
		imageview_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		
		
		String[] values = new String[]{"Feedback","Terms and Conditions","Privacy Policy"};
		
//		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.list_row,values);
		listView = (ListView)rootView.findViewById(R.id.list_settings);
		adapter =  new Adapter_Settings(getActivity(),values);
		listView.setAdapter(adapter);
//		
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (position == 0) {
					Feedback_fragment fragment =  new Feedback_fragment();
					((BaseContainerFragment) getParentFragment()).replaceFragment(
							fragment, true);
					
				}
				else if (position == 1) {
					Terms_fragment fragment = new Terms_fragment();
					((BaseContainerFragment) getParentFragment()).replaceFragment(
							fragment, true);
				}
				else if (position == 2) {
					Privacypolicy_fragment fragment = new Privacypolicy_fragment();
					((BaseContainerFragment) getParentFragment()).replaceFragment(
							fragment, true);
				}
			}
			
		});
		
		return rootView;
	}
	
	
	
	class Adapter_Settings extends BaseAdapter{
		private Context context;
		private LayoutInflater inflater;
		private String[] values;
		
		public Adapter_Settings(Context context, String[] values) {
			this.context = context;
			this.inflater = inflater;
			this.values = values;
			
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 3;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.list_row, null);
			
		}
			
			txt_item = (TextView)convertView.findViewById(R.id.txt_item);
			
			imageView = (ImageView)convertView.findViewById(R.id.img_arrow);
			 txt_item.setText(values[position]);
			return convertView;
		
	}

}
}