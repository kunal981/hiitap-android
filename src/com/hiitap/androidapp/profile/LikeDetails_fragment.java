package com.hiitap.androidapp.profile;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class LikeDetails_fragment extends Fragment {
	View rootView;
	ListView listView;
	String uuid, token, m_value, mstatus;
	Adapter_LikeDetails adapter_like;
	TextView txt_fname, txt_lname, mTextViewNoresult;
	ImageView img_profile, imageview_back;
	JSONArray mJsonArray;
	ProgressDialog pDialog;
	SharedPreferences sharedPreferences;
	Editor editor;
	List<MostThankedModel> mostThankedModels;
	MostThankedModel thankedModel;
	JSONObject jsonObject;
	ImageButton imgbtn_back;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_likepost, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		m_value = getArguments().getString("UUID_PASS");
		mstatus = getArguments().getString("LIKE");
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		listView = (ListView) rootView.findViewById(R.id.listview_Like_posts);
		mTextViewNoresult = (TextView) rootView.findViewById(R.id.txt_noresult);
		imageview_back = (ImageView) rootView.findViewById(R.id.imgtap);

		if (mstatus.matches("0")) {
			mTextViewNoresult.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
		} else {
			mTextViewNoresult.setVisibility(View.GONE);
			new ExecuteLikeTask().execute(AppConstant.BASE_URL,
					AppConstant.GET_POST_PROFILE_INFO, m_value, token);
		}

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				final MostThankedModel listDetailsModel = mostThankedModels
						.get(position);
				String mString = listDetailsModel.getUUID();
				Log.e("mString", "" + mString);
				ProfileFragment fragment = new ProfileFragment();

				Bundle savedInstanceState = new Bundle();
				savedInstanceState.putString("UUID_PASS", mString);
				fragment.setArguments(savedInstanceState);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);
			}
		});
		imgbtn_back = (ImageButton) rootView.findViewById(R.id.back_button);
		imgbtn_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		imageview_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
//
		return rootView;
	}

	public class Adapter_LikeDetails extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public Adapter_LikeDetails(Context context,
				List<MostThankedModel> mostThankedModels) {
			this.context = context;
			this.mostThankedModels = mostThankedModels;
			this.inflater = inflater;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mostThankedModels.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.listview_layout1, null);

			}
			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);
			txt_fname = (TextView) convertView.findViewById(R.id.txt_name);
			txt_fname.setText(listDetailsModel.getFirst_name() + " "
					+ listDetailsModel.getLast_name());

			img_profile = (ImageView) convertView.findViewById(R.id.flag);
			Picasso.with(getActivity())
					.load("https://www.hiitap.com/p/"
							+ listDetailsModel.getUUID()).into(img_profile);
			return convertView;

		}

	}

	class ExecuteLikeTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_post = WSConnector.getLikePosts(params[2], params[3]);
			return result_post;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			mostThankedModels = updateLike(jsonobjectString);
			sharedPrefernces();
			adapter_like = new Adapter_LikeDetails(getActivity(),
					mostThankedModels);
			listView.setAdapter(adapter_like);
		}

		private List<MostThankedModel> updateLike(String jsonobjectString) {
			mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					thankedModel = new MostThankedModel();
					jsonObject = mJsonArray.getJSONObject(i);
					String lastname = jsonObject.getString("last_name");
					String uuid = jsonObject.getString("person");
					Log.e("uuid inside loop======", "" + uuid);
					String firstname = jsonObject.getString("first_name");

					Log.e("jsonObject:", "" + jsonObject);

					thankedModel.setUUID(uuid);
					thankedModel.setFirst_name(firstname);
					thankedModel.setLast_name(lastname);

					mostThankedModels.add(thankedModel);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			return mostThankedModels;

		}

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();

		Log.e("token: ", "" + token);
		Log.e("uuid: ", "" + uuid);
		// editor.putString(AppConstant.USERNAME, username);
		// editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}
}
