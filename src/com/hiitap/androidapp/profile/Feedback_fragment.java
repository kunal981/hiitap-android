package com.hiitap.androidapp.profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.modal.Post_FeedbackModal;
import com.hiitap.androidapp.webservices.WSConnector;

public class Feedback_fragment extends Fragment {
	View rootView;
	ImageButton img_back;
	SharedPreferences sharedPreferences;
	Editor editor;
	String token, uuid, subject, details;
	EditText edt_topic, edt_details;
	Button btn_Send;
	ProgressDialog pDialog;
	JSONArray mJsonArray;
	Post_FeedbackModal feedbackModal;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(getActivity());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_feedback, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");

		img_back = (ImageButton) rootView.findViewById(R.id.back_button);
		img_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		edt_topic = (EditText) rootView.findViewById(R.id.edt_topic);
		edt_details = (EditText) rootView.findViewById(R.id.edt_details);
		btn_Send = (Button) rootView.findViewById(R.id.btn_sendfeedback);
		btn_Send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				subject = edt_topic.getText().toString();
				details = edt_details.getText().toString();
				boolean receive = validateSuccess(subject, details);
				if (receive) {
					 new Execute_FeedbackTask().execute(AppConstant.BASE_URL,
							 AppConstant.POST_FEEDBACK, token, subject, details);
				} else {
					Log.e("false----", "" + receive);
				}

				

			}

			private boolean validateSuccess(String subject, String details) {
				if (subject.length()<= 5) {
					showPopup("Feedback Status",
							"Please provide a Topic with at least 5 characters.");
					return false;
				}
				if (details.length()<=20) {
					showPopup("Feedback Status",
							"Please provide a detailed description with at least 20 characters.");
					return false;
				}
				return true;
			}

			private void showPopup(String title, String message) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertdialogPop = new AlertDialog.Builder(
						getActivity());
				alertdialogPop
						.setTitle(title)
						.setMessage(message)
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();

									}
								});
				AlertDialog dialog = alertdialogPop.create();
				dialog.show();
			}

		});

		return rootView;
	}

	class Execute_FeedbackTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_feedback = WSConnector.sendFeedback(params[2],
					params[3], params[4]);
			return result_feedback;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			pDialog.dismiss();
			feedbackModal = updateUI_feedback(jsonobjectString);

			sharedPrefernces();
			showDialog();

		}

		public void showDialog() {
			// TODO Auto-generated method stub
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(
					getActivity());
			alertDialog
					.setTitle("Feedback Status")
					.setMessage("Successfully Sent")
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int ID) {
									// TODO Auto-generated method stub
									edt_topic.setText("");
									edt_details.setText("");
									dialog.dismiss();
								}
							});
			AlertDialog dialog = alertDialog.create();
			dialog.show();
		}

		private Post_FeedbackModal updateUI_feedback(String jsonobjectString) {
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					feedbackModal = new Post_FeedbackModal();
					String UUID = jsonObject.getString("uuid");
					String status = jsonObject.getString("status");
					String person_uuid = jsonObject.getString("person.uuid");
					String subject = jsonObject.getString("subject");
					String details = jsonObject.getString("details");
					String created = jsonObject.getString("created");

					feedbackModal.setUUID(UUID);
					feedbackModal.setStatus(status);
					feedbackModal.setPerson_uuid(person_uuid);
					feedbackModal.setSubject(subject);
					feedbackModal.setDetails(details);
					feedbackModal.setCreated(created);

					edt_topic.setText(subject);
					edt_details.setText(details);

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return feedbackModal;

		}

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();

		Log.e("token: ", "" + token);
		Log.e("uuid: ", "" + uuid);
		// editor.putString(AppConstant.USERNAME, username);
		// editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}
}
