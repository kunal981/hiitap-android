package com.hiitap.androidapp.profile;

import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.hiitap.androidapp.R;

public class Share_Popup_activity extends Activity {

	Button btn_facebook, btn_twitter, btn_cancel;
	List<ResolveInfo> activityList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share_popup);
		btn_facebook.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, (String) v.getTag(R.string.app_name));
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, (String) v.getTag(R.drawable.ic_launcher));
				PackageManager pm = v.getContext().getPackageManager();
				activityList = pm.queryIntentActivities(shareIntent, 0);
				for (final ResolveInfo app : activityList) {
					
					if ((app.activityInfo.name).contains("facebook"));
					
					final ActivityInfo activity = app.activityInfo;
					
					final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
					
					shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
					
					shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
					
					shareIntent.setComponent(name);
					
					v.getContext().startActivity(shareIntent);
					
					break;
				}
				
			}
		});

	}

}
