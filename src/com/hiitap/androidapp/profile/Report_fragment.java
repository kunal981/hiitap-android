package com.hiitap.androidapp.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.webservices.WSConnector;

public class Report_fragment extends Fragment {
	EditText edt_text;
	InputMethodManager imm;
	Button btn_send;
	String message, uuid, token, message_uuid;
	SharedPreferences sharedPreferences;
	Editor editor;
	ProgressDialog pDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		pDialog = new ProgressDialog(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_report, container,
				false);
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		message_uuid = getArguments().getString("UUID_MESSAGE");
		btn_send = (Button) rootView.findViewById(R.id.btn_comments);
		edt_text = (EditText) rootView.findViewById(R.id.edt_comment_report);
		edt_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				edt_text.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						// imm = (InputMethodManager) getActivity()
						// .getSystemService(Context.INPUT_METHOD_SERVICE);
						// imm.showSoftInput(edt_text,
						// InputMethodManager.SHOW_IMPLICIT);
						try {
							imm = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.showSoftInput(edt_text,
									InputMethodManager.SHOW_IMPLICIT);
						} catch (Exception e) {
							// TODO: handle exception
							e.getMessage();
						}

					}
				});
			}
		});
		edt_text.requestFocus();

		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				message = edt_text.getText().toString();
				sharedPreferences();
				new ExecuteReportTask().execute(AppConstant.BASE_URL,
						AppConstant.POST_REPORT, uuid,message_uuid,message);

			}
		});

		return rootView;
	}

	class ExecuteReportTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = WSConnector.postReport(params[2], params[3],params[4]);
			return result;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog.setMessage("Loading...");
			pDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			showDialog();
		}

	}
	public void showDialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				getActivity());
		alertDialog
				.setTitle("Success")
				.setMessage("Request successfully sent")
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int ID) {
								// TODO Auto-generated method stub
								
								dialog.dismiss();
								getActivity().onBackPressed();
							}
						});
		AlertDialog dialog = alertDialog.create();
		dialog.show();
	}

	public void sharedPreferences() {
		editor = sharedPreferences.edit();

		Log.e("token: ", "" + token);
		Log.e("uuid: ", "" + uuid);
		// editor.putString(AppConstant.USERNAME, username);
		// editor.putString(AppConstant.PASSWORD, password);
		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);

		editor.commit();
	}

}
