package com.hiitap.androidapp.profile;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.modal.UserModal;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class Followers_fragment extends Fragment {
	ListView listView;
	Adapter_Follower adapter_follower;
	String uuid, token, m_value;
	JSONArray mJsonArray;
	TextView txt_fname, txt_lname;
	ImageView img_profile,img_tap;
	SharedPreferences sharedPreferences;
	Editor editor;
	UserModal modal;
	ProgressDialog pDialog;
	List<MostThankedModel> mostThankedModels;
	ImageButton img_backbutton;
	MostThankedModel thankedModel;
	View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_followers, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		// m_value = getArguments().getString("UUID_PASS");
		pDialog = new ProgressDialog(getActivity());
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		uuid = sharedPreferences.getString(AppConstant.UUID, "");
		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		img_tap = (ImageView) rootView.findViewById(R.id.imgtap);
		img_backbutton = (ImageButton) rootView.findViewById(R.id.back_button);
		img_backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		img_tap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		listView = (ListView) rootView.findViewById(R.id.list_view_follower);
		new ExecuteFollowersTask().execute(AppConstant.BASE_URL,
				AppConstant.GET_FOLLOWERS, uuid, token);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				final MostThankedModel listDetailsModel = mostThankedModels
						.get(position);
				String mString = listDetailsModel.getUUID();
				ProfileFragment fragment = new ProfileFragment();
				Bundle savedInstanceState = new Bundle();
				savedInstanceState.putString("UUID_PASS", mString);
				fragment.setArguments(savedInstanceState);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);
			}
		});

		return rootView;
	}

	public class Adapter_Follower extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public Adapter_Follower(Context context,
				List<MostThankedModel> mostThankedModels) {
			this.context = context;
			this.inflater = inflater;
			this.mostThankedModels = mostThankedModels;
		}

		// public Adapter1(FragmentActivity activity) {
		// // TODO Auto-generated constructor stub
		// }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mostThankedModels.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.listview_layout1, null);

			}
			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);
			txt_fname = (TextView) convertView.findViewById(R.id.txt_name);
			txt_fname.setText(listDetailsModel.getFirst_name() + " "
					+ listDetailsModel.getLast_name());

			img_profile = (ImageView) convertView.findViewById(R.id.flag);
			Picasso.with(getActivity())
					.load("https://www.hiitap.com/p/"
							+ listDetailsModel.getUUID()).into(img_profile);
			return convertView;

		}

	}

	class ExecuteFollowersTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			return WSConnector.getFollower(params[2], params[3]);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			mostThankedModels = updateUI_Followers(jsonobjectString);
			sharedPrefernces();
			adapter_follower = new Adapter_Follower(getActivity(),
					mostThankedModels);
			listView.setAdapter(adapter_follower);
		}

		private List<MostThankedModel> updateUI_Followers(
				String jsonobjectString) {
			// TODO Auto-generated method stub
			mostThankedModels = new ArrayList<MostThankedModel>();
			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {
					thankedModel = new MostThankedModel();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);
					String uuid = jsonObject.getString("uuid");
					String firstname = jsonObject.getString("first_name");
					String lastname = jsonObject.getString("last_name");
					Log.e("jsonObject:", "" + jsonObject);

					thankedModel.setUUID(uuid);
					thankedModel.setFirst_name(firstname);
					thankedModel.setLast_name(lastname);

					mostThankedModels.add(thankedModel);

					// txt_fname.setText(firstname);
					// Picasso.with(getActivity())
					// .load("https://www.hiitap.com/p/" + uuid)
					// .into(img_profile);
					//
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mostThankedModels;

		}

		public void sharedPrefernces() {
			editor = sharedPreferences.edit();

			Log.e("token: ", "" + token);
			Log.e("uuid: ", "" + uuid);
			// editor.putString(AppConstant.USERNAME, username);
			// editor.putString(AppConstant.PASSWORD, password);
			editor.putString(AppConstant.TOKEN, token);
			editor.putString(AppConstant.UUID, uuid);

			editor.commit();
		}
	}

}