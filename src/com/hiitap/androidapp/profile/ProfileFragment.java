package com.hiitap.androidapp.profile;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hiitap.androidapp.AppConstant;
import com.hiitap.androidapp.BaseConvert;
import com.hiitap.androidapp.R;
import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.home.Comment_Fragment;
import com.hiitap.androidapp.modal.MostThankedModel;
import com.hiitap.androidapp.modal.UserModal;
import com.hiitap.androidapp.tap.TapFragmentSpinner;
import com.hiitap.androidapp.util.Utility;
import com.hiitap.androidapp.webservices.WSConnector;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment {

	ListView listView;
	ListAdapter1 adapter1, adapter2;

	ImageView imgProfile;
	TextView txt_username, txt_send, txt_msz, txt_like;
	Resources resources;
	PackageManager packageManager;

	ResolveInfo ri;
	Intent openInChooser;
	Intent sendIntent;
	String picturePath, uuid, username, password, token, localUUID, action,
			counter, firstname, post, person, like, like_count, follow,
			lastname, first_name, base64path;
	View rootView;
	Button btn_comment, btn_send, btn_msz, btnSendtap, btn_follow, btn_refresh,
			btn_settings, btn_following_Count, btn_followers_count,
			btnFollowing, btnFollowers, btnTapIcon, btn_backbutton;
	JSONObject jsonObject;

	UserModal modal;
	JSONArray mJsonArray;
	SharedPreferences sharedPreferences;
	Editor editor;
	ProgressDialog pDialog;
	List<MostThankedModel> mostThankedModels;
	List<MostThankedModel> listSend;
	List<MostThankedModel> listReceive;
	MostThankedModel thankedModel;
	RelativeLayout relative_buttons;

	private static int RESULT_LOAD_IMAGE = 1;
	boolean isClicked = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		pDialog = new ProgressDialog(getActivity());
		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.HII_TAP, Context.MODE_PRIVATE);
		// localUUID = sharedPreferences.getString(AppConstant.UUID, "");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_profile, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}

		relative_buttons = (RelativeLayout) rootView
				.findViewById(R.id.relative_buttons);
		Log.i("STARTING uuid: ", "" + uuid);

		token = sharedPreferences.getString(AppConstant.TOKEN, "");
		listView = (ListView) rootView
				.findViewById(R.id.listviewProfileActivity);

		listView.setVisibility(View.VISIBLE);
		btn_send = (Button) rootView.findViewById(R.id.btnSend);
		btn_send.setSelected(true);
		btn_msz = (Button) rootView.findViewById(R.id.btnMessage);
		btnTapIcon = (Button) rootView.findViewById(R.id.btnTapIcon);
		btn_backbutton = (Button) rootView.findViewById(R.id.backButton);
		imgProfile = (ImageView) rootView.findViewById(R.id.img_ProfilePic);
		btn_refresh = (Button) rootView.findViewById(R.id.btnRefresh);
		btn_settings = (Button) rootView.findViewById(R.id.btnSettings);
		btnSendtap = (Button) rootView.findViewById(R.id.btnSendTAP);
		btn_following_Count = (Button) rootView
				.findViewById(R.id.btn_following_Count);
		txt_username = (TextView) rootView.findViewById(R.id.txt_Username);
		btn_followers_count = (Button) rootView.findViewById(R.id.btn_msgCount);
		btnFollowing = (Button) rootView
				.findViewById(R.id.btn_following_profile);
		btnFollowers = (Button) rootView
				.findViewById(R.id.btn_followers_profile);
		btn_follow = (Button) rootView.findViewById(R.id.btn_following_tap);
		btnTapIcon.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		btn_backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		imgProfile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});

		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// // TODO Auto-generated method stub
				// btn_send.setBackgroundResource(R.drawable.sendblack);
				// btn_msz.setBackgroundResource(R.drawable.msz1);
				if (!btn_send.isSelected()) {
					btn_send.setSelected(true);
					btn_msz.setSelected(false);
				}

				imgSend();
			}
		});
		btn_msz.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// btn_send.setBackgroundResource(R.drawable.send);
				// btn_msz.setBackgroundResource(R.drawable.msz);
				if (!btn_msz.isSelected()) {
					btn_msz.setSelected(true);
					btn_send.setSelected(false);
				}

				imgMessage();
			}
		});

		btn_refresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				new UpdateProfilePost().execute(AppConstant.BASE_URL,
						AppConstant.GET_PROFILE_POSTS, uuid, token);
				Toast.makeText(getActivity(), "Refreshing..", Toast.LENGTH_LONG)
						.show();

			}
		});

		btn_settings.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Settings_profile_fragment settings_fragment = new Settings_profile_fragment();

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						settings_fragment, true);
			}

		});

		btnSendtap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String firstname = modal.getFirst_name();
				String lastname = modal.getLast_name();
				String uuid = modal.getUUID();
				TapFragmentSpinner fragment = new TapFragmentSpinner();
				Bundle savedInstanceState = new Bundle();
				savedInstanceState.putString("UUID_PASSED", uuid);
				savedInstanceState.putString("FIRSTNAME", firstname);
				savedInstanceState.putString("LASTNAME", lastname);

				fragment.setArguments(savedInstanceState);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);

			}
		});

		modal = new UserModal();

		// txt_send = (TextView) rootView.findViewById(R.id.txt_send);
		// txt_msz = (TextView) rootView.findViewById(R.id.txt_msz);

		if (getArguments() != null) {

			Bundle bundle = this.getArguments();
			uuid = bundle.getString("UUID_PASS");
			relative_buttons.setVisibility(View.VISIBLE);
			btn_backbutton.setVisibility(View.VISIBLE);
			btn_settings.setVisibility(View.GONE);

		} else {
			uuid = sharedPreferences.getString(AppConstant.UUID, "");
			relative_buttons.setVisibility(View.GONE);
			btn_backbutton.setVisibility(View.GONE);
			btn_settings.setVisibility(View.VISIBLE);

		}
		new ExecuteLineartabs().execute(AppConstant.BASE_URL,
				AppConstant.GET_PROFILE, uuid, token);

		btnFollowing.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// String UUID = getString(resId)
				Following_fragment following_fragment = new Following_fragment();

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						following_fragment, true);

			}
		});

		btnFollowers.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Followers_fragment followers_fragment = new Followers_fragment();

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						followers_fragment, true);

			}
		});

		btn_follow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String follow_status = modal.getFollows();

				String text_status = "false";
				if (follow_status.equals("false")) {
					text_status = "true";
					new ExecuteTAPbutton().execute(AppConstant.BASE_URL,
							AppConstant.TAP_FOLLOW, uuid, token, text_status);

				} else if (follow_status.equals("true")) {
					text_status = "false";
					new ExecuteTAPbutton().execute(AppConstant.BASE_URL,
							AppConstant.TAP_FOLLOW, uuid, token, text_status);
				}

			}
		});

		return rootView;
	}

	class ListAdapter1 extends BaseAdapter {
		private Context context;
		private LayoutInflater inflater;
		List<MostThankedModel> mostThankedModels;

		public ListAdapter1(Context context,
				List<MostThankedModel> mostThankedModels) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.mostThankedModels = mostThankedModels;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return mostThankedModels.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mostThankedModels.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			if (inflater == null) {
				inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			}

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.activity_message_send_details, null);
				holder = new ViewHolder();

				holder.txtCommentProfile = (TextView) convertView
						.findViewById(R.id.txt_bubble_profile);

				holder.txtbubble = (TextView) convertView
						.findViewById(R.id.txt_bubble_profile);
				holder.txtcomment = (TextView) convertView
						.findViewById(R.id.txt_comment);
				holder.txtshare = (TextView) convertView
						.findViewById(R.id.txt_share);
				holder.txtreport = (TextView) convertView
						.findViewById(R.id.txt_report);
				holder.btn_hearticon = (Button) convertView
						.findViewById(R.id.btn_hearticon);
				holder.txtlarge = (TextView) convertView
						.findViewById(R.id.txt_desc);
				holder.txtsmallcomment = (TextView) convertView
						.findViewById(R.id.txt_small_comments);
				holder.txtdate = (TextView) convertView
						.findViewById(R.id.txt_date);
				holder.img_profile = (ImageView) convertView
						.findViewById(R.id.flag);
				holder.txtlike = (TextView) convertView
						.findViewById(R.id.txt_like);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final MostThankedModel listDetailsModel = mostThankedModels
					.get(position);

			holder.txtlarge.setText(listDetailsModel.getAction() + " " + "sent"
					+ " " + "to" + " "
					+ listDetailsModel.getToUser().getFirst_name() + " "
					+ listDetailsModel.getToUser().getLast_name());

			holder.txtsmallcomment.setText(listDetailsModel.getMessage());
			String time = Utility.convertTime(listDetailsModel.getTime());
			holder.txtdate.setText(time);

			String userId = listDetailsModel.getToUser().getUUID();
			if (userId != null) {
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/" + userId)
						.into(holder.img_profile);
			}

			holder.txtbubble.setText("Comment" + " "
					+ listDetailsModel.getComment_count());

			// getTextViewResult();
			holder.txtlike.setText("Likes" + " "
					+ listDetailsModel.getLike_count());
			// holder.imgMore.setTag(position);
			holder.btnMore = (Button) convertView.findViewById(R.id.btnMore);
			holder.image_test = (ImageView) convertView
					.findViewById(R.id.test_image);
			final String getImage = listDetailsModel.getImage();

			if (getImage != null) {
				holder.btnMore.setVisibility(View.VISIBLE);
				Picasso.with(getActivity()).load(getImage)
						.into(holder.image_test);
				
				holder.btnMore.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						holder.image_test.setVisibility(View.VISIBLE);
						holder.btnMore.setVisibility(View.INVISIBLE);

					}
				});

			} else {
				holder.btnMore.setVisibility(View.INVISIBLE);
			}

			if (listDetailsModel.getLiked().matches("true")) {
				holder.btn_hearticon.setBackgroundResource(R.drawable.heart);

			} else {
				holder.btn_hearticon.setBackgroundResource(R.drawable.heart1);
			}
			holder.txtlike.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String firstUUID = listDetailsModel.getUUID();

					String mString_like = listDetailsModel.getLike_count();

					LikeDetails_fragment details_fragment = new LikeDetails_fragment();
					Bundle savedInstanceState = new Bundle();
					savedInstanceState.putString("UUID_PASS", firstUUID);
					savedInstanceState.putString("LIKE", mString_like);

					details_fragment.setArguments(savedInstanceState);
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(details_fragment, true);

				}
			});

			holder.btn_hearticon.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// if (isClicked) {
					String firstUUID = listDetailsModel.getUUID();
					String postTo = listDetailsModel.getToUser().getUUID();
					String like = listDetailsModel.getLiked();

					String txt_ = listDetailsModel.getLike_count();
					String like_status = "false";
					if (like.equals("false")) {
						like_status = "true";

						mostThankedModels.get(position).setLiked(like_status);

						int myNum = Integer.parseInt(txt_) + 1;

						mostThankedModels.get(position).setLike_count(
								String.valueOf(myNum));

						notifyDataSetChanged();
						new ExcecuteLikePosts().execute(AppConstant.BASE_URL,
								AppConstant.LIKE_POSTS, token, firstUUID,
								postTo, like_status);

					} else if (like.equals("true")) {

						like_status = "false";

						mostThankedModels.get(position).setLiked(like_status);

						int myNum = Integer.parseInt(txt_) - 1;

						mostThankedModels.get(position).setLike_count(
								String.valueOf(myNum));

						notifyDataSetChanged();
						new ExcecuteLikePosts().execute(AppConstant.BASE_URL,
								AppConstant.LIKE_POSTS, token, firstUUID,
								postTo, like_status);
					}

				}
			});

			holder.txtCommentProfile
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String getUuid = listDetailsModel.getUUID();
							String mString_comment = listDetailsModel
									.getComment_count();
							String personUUID = listDetailsModel.getToUser()
									.getUUID();
							String comment_count = listDetailsModel
									.getComment_count();
							Comment_Fragment comment_Fragment = new Comment_Fragment();
							Bundle savedInstanceState = new Bundle();
							savedInstanceState.putString("UUID_PASS", getUuid);
							savedInstanceState.putString("COMMENT",
									mString_comment);
							savedInstanceState.putString("PERSON_UUID",
									personUUID);
							savedInstanceState.putString("COMMENT_COUNT",
									comment_count);
							comment_Fragment.setArguments(savedInstanceState);
							((BaseContainerFragment) getParentFragment())
									.replaceFragment(comment_Fragment, true);

						}
					});

			holder.txtshare.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Resources resources = getResources();

					Intent emailIntent = new Intent();
					emailIntent.setAction(Intent.ACTION_SEND);
					// Native email client doesn't currently support HTML,
					// but
					// it doesn't hurt to try in case they fix it
					emailIntent.putExtra(Intent.EXTRA_TEXT, Html
							.fromHtml(resources
									.getString(R.string.share_email_native)));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT,
							resources.getString(R.string.share_email_subject));
					emailIntent.setType("message/rfc822");
					PackageManager pm = getActivity().getPackageManager();
					Intent sendIntent = new Intent(Intent.ACTION_SEND);
					sendIntent.setType("text/plain");

					Intent openInChooser = Intent.createChooser(emailIntent,
							resources.getString(R.string.share_chooser_text));

					List<ResolveInfo> resInfo = pm.queryIntentActivities(
							sendIntent, 0);
					List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
					for (int i = 0; i < resInfo.size(); i++) {
						// Extract the label, append it, and repackage it in
						// a
						// LabeledIntent
						ResolveInfo ri = resInfo.get(i);
						String packageName = ri.activityInfo.packageName;
						if (packageName.contains("android.email")) {
							emailIntent.setPackage(packageName);
						} else if (packageName.contains("twitter")
								|| packageName.contains("facebook")

						) {
							Intent intent = new Intent();
							intent.setComponent(new ComponentName(packageName,
									ri.activityInfo.name));
							intent.setAction(Intent.ACTION_SEND);
							intent.setType("text/plain");
							if (packageName.contains("twitter")) {
								intent.putExtra(Intent.EXTRA_TEXT, resources
										.getString(R.string.share_twitter));
							} else if (packageName.contains("facebook")) {

								intent.putExtra(Intent.EXTRA_TEXT, resources
										.getString(R.string.share_facebook));
							}

							intentList.add(new LabeledIntent(intent,
									packageName, ri.loadLabel(pm), ri.icon));
						}
					}

					// convert intentList to array
					LabeledIntent[] extraIntents = intentList
							.toArray(new LabeledIntent[intentList.size()]);

					openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS,
							extraIntents);
					startActivity(openInChooser);
				}
			});
			holder.txtreport.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final MostThankedModel model = mostThankedModels
							.get(position);
					String message = model.getUUID();
					Report_fragment report_fragment = new Report_fragment();
					Bundle savedInstanceState = new Bundle();
					savedInstanceState.putString("UUID_MESSAGE", message);
					report_fragment.setArguments(savedInstanceState);
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(report_fragment, true);
				}
			});

			return convertView;
		}

	}

	static class ViewHolder {
		ImageButton imgIcon;
		TextView txtlarge, txtcomment, txtdate, txtlike, txtbubble, txtshare,
				txtreport, txtsmallcomment, txtCommentProfile;
		ImageView image_test, img_profile;
		Button btn_hearticon, btnMore;
	}

	public void imgSend() {
		// TODO Auto-generated method stub
		listView.setAdapter(adapter1);

	}

	public void imgMessage() {
		// TODO Auto-generated method stub

		listView.setAdapter(adapter2);
		btn_msz.setClickable(true);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		if (resultCode == Activity.RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			picturePath = cursor.getString(columnIndex);
			cursor.close();
			try {
				// hide video preview

				// bimatp factory
				BitmapFactory.Options options = new BitmapFactory.Options();

				// downsizing image as it throws OutOfMemory Exception for
				// larger
				// images
				options.inSampleSize = 1;

				Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
				Bitmap mBitmap = Bitmap.createScaledBitmap(bitmap, 200, 200,
						false);

				// Bitmap mBitmap_send = StringToBitMap(picturePath);
				// Log.e("mBitmap_send: ", ""+mBitmap_send);
				imgProfile.setImageBitmap(mBitmap);
				base64path = convert_image(mBitmap);
				new updateProfilePic().execute(AppConstant.BASE_URL,
						AppConstant.SAVE_PROFILE_PIC, uuid, token, base64path,
						"png");

			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			// base64path=setPic(picturePath);

		}
	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Log.e("STREAM", "" + stream);
		// Log.e("BITMAP: ", "" + bitmap);
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to
		// which
		// format
		// you want.

		byte[] byte_arr = stream.toByteArray();
		// long lenthbmp = byte_arr.length;
		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	class updateProfilePic extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = WSConnector.SaveProfilePic(params[2], params[3],
					params[4], params[5]);
			return result;
		}
		
		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			//updateProfilePic(jsonobjectString);
		}

		
	}
	


	// private void setPic() {
	// // TODO Auto-generated method stub
	// int targetWidth = imgProfile.getWidth();
	// int targetHeight = imgProfile.getHeight();
	//
	// BitmapFactory.Options options = new BitmapFactory.Options();
	// options.inJustDecodeBounds = true;
	// BitmapFactory.decodeFile(picturePath, options);
	//
	// int photoWidth = options.outWidth;
	// int photoHeight = options.outHeight;
	//
	// int scaleFactor = Math.min(photoWidth / targetWidth, photoHeight
	// / targetHeight);
	//
	// options.inJustDecodeBounds = false;
	// options.inSampleSize = scaleFactor;
	// options.inPurgeable = true;
	// Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
	// imgProfile.setImageBitmap(bitmap);
	//
	// }

	class ExecuteLineartabs extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_profile = WSConnector
					.getProfile(params[2], params[3]);
			return result_profile;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			pDialog.setMessage("Loading...");

			pDialog.show();
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			new UpdateProfilePost().execute(AppConstant.BASE_URL,
					AppConstant.GET_PROFILE_POSTS, uuid, token);

			sharedPrefernces();
			modal = updateUI(jsonobjectString);

		}

	}

	public UserModal updateUI(String jsonobjectString) {
		// TODO Auto-generated method stub
		try {
			mJsonArray = new JSONArray(jsonobjectString);
			for (int i = 0; i < mJsonArray.length(); i++) {
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				modal = new UserModal();

				String uuid = jsonObject.getString("uuid");
				firstname = jsonObject.getString("first_name");
				Log.e("firstname:", "" + firstname);
				String lastname = jsonObject.getString("last_name");
				Log.e("lastname:", "" + lastname);
				String sent = jsonObject.getString("sent");
				String received = jsonObject.getString("received");
				String followers = jsonObject.getString("followers");
				String following = jsonObject.getString("following");
				String follows = jsonObject.getString("follows");
				txt_username.setText((firstname) + " " + (lastname));
				btn_send.setText(sent);
				btn_msz.setText(received);
				btn_following_Count.setText(following);
				btn_followers_count.setText(followers);
				Picasso.with(getActivity())
						.load("https://www.hiitap.com/p/" + uuid)
						.into(imgProfile);
				btnSendtap.setText("Send TAP to " + " " + firstname);
				if (follows.matches("false")) {
					btn_follow.setText("Follow");
				} else {
					btn_follow.setText("Following");
				}

				modal.setUUID(uuid);
				modal.setFirst_name(firstname);
				modal.setLast_name(lastname);
				modal.setSent(sent);
				modal.setFollowers(followers);
				modal.setReceived(received);
				modal.setFollowing(following);
				modal.setFollows(follows);

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return modal;

	}

	class UpdateProfilePost extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = WSConnector.getProfilePosts(params[2], params[3]);
			return result;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);

			mostThankedModels = updatedUI(jsonobjectString);
			getdetailsList(mostThankedModels);

		}

		public List<MostThankedModel> updatedUI(String jsonobjectString) {
			// TODO Auto-generated method stub
			List<MostThankedModel> mostThankedModels = new ArrayList<MostThankedModel>();

			try {
				mJsonArray = new JSONArray(jsonobjectString);
				for (int i = 0; i < mJsonArray.length(); i++) {

					MostThankedModel thankedModel = new MostThankedModel();
					// UserModal userModal = new UserModal();
					JSONObject jsonObject = mJsonArray.getJSONObject(i);

					String uuid = jsonObject.getString("uuid");
					String action = jsonObject.getString("action");
					String message = jsonObject.getString("message");
					String like_count = jsonObject.getString("like_count");
					String comment_count = jsonObject
							.getString("comment_count");
					String time = jsonObject.getString("time");
					String image = null;
					if (jsonObject.has("image")) {
						image = jsonObject.getString("image");
						thankedModel.setImage(image);

					}
					String liked = jsonObject.getString("liked");
					// String from_action = jsonObject.getString("action");

					thankedModel.setUUID(uuid);
					thankedModel.setAction(action);
					thankedModel.setMessage(message);
					thankedModel.setLike_count(like_count);
					thankedModel.setComment_count(comment_count);
					thankedModel.setTime(time);
					thankedModel.setLiked(liked);

					// thankedModel.setImage(image);

					UserModal toUserModel = new UserModal();
					JSONObject jsonInnerToOject = jsonObject
							.getJSONObject("to");
					String uuidtoInner = jsonInnerToOject.getString("uuid");
					String fNameTo = jsonInnerToOject.getString("first_name");
					String lNameTo = jsonInnerToOject.getString("last_name");
					// Todo get remaining items
					toUserModel.setUUID(uuidtoInner);
					toUserModel.setFirst_name(fNameTo);
					toUserModel.setLast_name(lNameTo);

					thankedModel.setToUser(toUserModel);

					// Todo set usermodel for from
					UserModal fromUserModal = new UserModal();
					JSONObject jsonfromInner = jsonObject.getJSONObject("from");
					String uuidFrom = jsonfromInner.getString("uuid");
					String fnameFrom = jsonfromInner.getString("first_name");
					String lNameFrom = jsonfromInner.getString("last_name");
					fromUserModal.setUUID(uuidFrom);
					fromUserModal.setFirst_name(fnameFrom);
					fromUserModal.setLast_name(lNameFrom);

					thankedModel.setFromUser(fromUserModal);

					mostThankedModels.add(thankedModel);

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return mostThankedModels;

		}
	}

	public void getdetailsList(List<MostThankedModel> mostThankedModels) {
		// TODO Auto-generated method stub
		listSend = new ArrayList<MostThankedModel>();
		listReceive = new ArrayList<MostThankedModel>();
		for (int i = 0; i < mostThankedModels.size(); i++) {
			// Log.e("iii",""+mostThankedModels);

			MostThankedModel mostThankedModel = mostThankedModels.get(i);
			String toUser = mostThankedModel.getToUser().getUUID();
			if (toUser.equals(uuid)) {

				listSend.add(mostThankedModel);
			} else {

				listReceive.add(mostThankedModel);
			}
		}

		adapter1 = new ListAdapter1(getActivity(), listReceive);
		adapter2 = new ListAdapter1(getActivity(), listSend);
		listView.setAdapter(adapter2);
		listView.setAdapter(adapter1);

	}

	class ExcecuteLikePosts extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_like_post = WSConnector.LikePosts(params[2],
					params[3], params[4], params[5]);

			return result_like_post;
		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			// mostThankedModels = update_like_UI(jsonobjectString);
		}

	}

	public List<MostThankedModel> update_like_UI(String jsonobjectString) {
		// TODO Auto-generated method stub
		List<MostThankedModel> mostThankedModels = new ArrayList<MostThankedModel>();

		try {
			mJsonArray = new JSONArray(jsonobjectString);

			for (int i = 0; i < mJsonArray.length(); i++) {
				MostThankedModel thankedModel = new MostThankedModel();
				JSONObject jsonObject = mJsonArray.getJSONObject(i);
				post = jsonObject.getString("post");

				counter = jsonObject.getString("counter");

				thankedModel.setCounter(counter);
				thankedModel.setPost(post);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mostThankedModels;

	}

	class ExecuteTAPbutton extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result_tap_follow = WSConnector.TAP_Follow(params[2],
					params[3], params[4]);
			return result_tap_follow;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			btn_follow.setText("Follow");
		}

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();

		editor.putString(AppConstant.TOKEN, token);
		editor.putString(AppConstant.UUID, uuid);
		editor.putString(AppConstant.FIRST_NAME, firstname);

		editor.commit();
	}
}
