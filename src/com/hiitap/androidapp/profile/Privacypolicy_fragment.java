package com.hiitap.androidapp.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.hiitap.androidapp.R;

public class Privacypolicy_fragment extends Fragment{
	View rootView;
	ImageButton img_back;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragmnet_privacypolicy, container,
					false);
		} else {
			((ViewGroup) rootView.getParent()).removeView(rootView);
			return rootView;
		}
		img_back=(ImageButton)rootView.findViewById(R.id.back_button);
		img_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		return rootView;
	}

}
