package com.hiitap.androidapp.base;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hiitap.androidapp.R;
import com.hiitap.androidapp.home.HomeFragment;

public class HomeContainerFragment extends BaseContainerFragment {

	private boolean mIsViewInited;
	private static final String TAG = "HomeContainerFragment";
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.container_framelayout, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i(TAG, "tab 1 container on activity created");
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		} else {
			getChildFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}

	}	

	private void initView() {
		Log.i(TAG, "tab 1 init view");
		HomeFragment homeFragment = new HomeFragment();
		replaceFragment(homeFragment, false);
	}

	
}
