package com.hiitap.androidapp.base;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.hiitap.androidapp.R;

public class BaseContainerFragment extends Fragment {
	public Fragment currentFragment;

	public void replaceFragment(Fragment fragment, boolean addToBackStack) {
		// TODO Auto-generated method stub

		FragmentTransaction transaction = getChildFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.container_framelayout, fragment);
		currentFragment = fragment;
		if (addToBackStack) {
			transaction.addToBackStack(null);
		}
		transaction.commit();
		getChildFragmentManager().executePendingTransactions();
	}

	public boolean popFragment() {

		Log.i("Base", "pop fragment: "
				+ getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();

		}

		return isPop;
	}

}
