package com.hiitap.androidapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.hiitap.androidapp.webservices.WSConnector;

public class SignUp_Activity extends Activity {
	Button register;
	SharedPreferences sharedPreferences;
	Editor editor;
	String first_name, last_name, email, confirmEmail, pwd, confirmPwd;
	EditText edtfName, edtlName, edtemail, edtconfirmEmail, edtpwd,
			edtconfirmPwd;
	ProgressDialog pDialog;
	ImageButton imageButton;
	ImageView imageView;
	TextView txt_Description;
	SpannableString spannableString;
	ClickableSpan clickableSpan, span;
	CheckBox checkBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

		sharedPreferences = getSharedPreferences(AppConstant.HII_TAP,
				Context.MODE_PRIVATE);

		pDialog = new ProgressDialog(SignUp_Activity.this);
		edtfName = (EditText) findViewById(R.id.edt_fname);
		edtlName = (EditText) findViewById(R.id.edt_lname);
		edtemail = (EditText) findViewById(R.id.edt_Email);
		edtconfirmEmail = (EditText) findViewById(R.id.edt_confirm_Email);
		edtpwd = (EditText) findViewById(R.id.edt_pwd);
		edtconfirmPwd = (EditText) findViewById(R.id.edt_confirm_pwd);
		imageButton = (ImageButton) findViewById(R.id.back_button_signup);
		imageView = (ImageView) findViewById(R.id.img_tap);
		txt_Description = (TextView) findViewById(R.id.txt_Description);
		register = (Button) findViewById(R.id.btn_register);
		checkBox = (CheckBox) findViewById(R.id.checkBox1);

		spannableString = new SpannableString(
				"I have read and agreed to the Privacy Policy and Terms of Service");
		spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 30, 44, 0);
		spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 49, 65, 0);

		clickableSpan = new ClickableSpan() {

			@Override
			public void onClick(View widget) {
				// TODO Auto-generated method stub

				Intent i = new Intent(SignUp_Activity.this,
						Privacy_policy.class);
				startActivity(i);

			}
		};
		spannableString.setSpan(clickableSpan, 30, 44, 0);
		txt_Description.setText(spannableString);
		txt_Description.setMovementMethod(LinkMovementMethod.getInstance());

		txt_Description.setText(spannableString, BufferType.SPANNABLE);

		span = new ClickableSpan() {

			@Override
			public void onClick(View widget) {
				// TODO Auto-generated method stub
				Intent i1 = new Intent(SignUp_Activity.this,
						Terms_conditions.class);
				startActivity(i1);

			}
		};
		spannableString.setSpan(span, 49, 65, 0);
		txt_Description.setText(spannableString);
		txt_Description.setMovementMethod(LinkMovementMethod.getInstance());

		txt_Description.setText(spannableString, BufferType.SPANNABLE);

		imageButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignUp_Activity.this,
						WelcomePage_Activity.class);
				startActivity(i);
				finish();
			}
		});

		imageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignUp_Activity.this,
						WelcomePage_Activity.class);
				startActivity(i);
				finish();
			}
		});

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				first_name = edtfName.getText().toString().trim();
				last_name = edtlName.getText().toString();
				email = edtemail.getText().toString();
				confirmEmail = edtconfirmEmail.getText().toString();
				pwd = edtpwd.getText().toString().trim();
				confirmPwd = edtconfirmPwd.getText().toString();
				boolean receive = validatation_method(first_name, last_name,
						email, confirmEmail, pwd, confirmPwd, checkBox);
				if (receive) {
					new ExecuteTask()
							.execute(first_name, last_name, email, pwd);
				}

			}

			private boolean validatation_method(String first_name,
					String last_name, String email, String pwd,
					String confirmEmail, String confirmPwd, CheckBox checkBox) {
				// TODO Auto-generated method stub
				if (edtfName != null
						&& edtfName.getText().toString().trim().length() < 3) {
					edtfName.setError("Please enter your first name");
					return false;
				}
				if (edtlName != null
						&& edtlName.getText().toString().trim().length() < 3) {
					edtlName.setError("Please enter your last name");
					return false;
				}
				if (!isValidEmail(email)) {
					edtemail.setError("Please enter an valid email address");
					return false;
				}
//				if (isValidEmail(email)) {
//					edtconfirmEmail
//							.setError("The Confirm email must match your email address");
//					return false;
//				}
				if (!edtconfirmEmail.getText().toString()
						.matches(edtemail.getText().toString())) {
					edtconfirmEmail
							.setError("Please confirm your email address");
					return false;
				}

				if (!isValidPassword(pwd)) {
					edtpwd.setError("Please enter minimum 6 characters in password field");
					return false;
				}
//				if (isValidPassword(pwd)) {
//					edtconfirmPwd
//							.setError("Password does not match the confirm password");
//					return false;
//				}
				if (!edtconfirmPwd.getText().toString().trim()
						.matches(edtpwd.getText().toString())) {
					edtconfirmPwd.setError("Please  confirm your password");
					return false;
				}
//				if (edtconfirmPwd.getText().toString().trim().matches(edtconfirmPwd.getText().toString())) {
//					edtconfirmPwd.setError("Password does not match the confirm password");
//					return false;
//				}
				
				if (!checkBox.isChecked()) {
					checkBox.setError("Please accept the terms and conditions");
					return false;
				}
				return true;
			}
		});

	}

	protected boolean isValidPassword(String pwd) {
		// TODO Auto-generated method stub
		if (edtpwd != null && edtpwd.getText().toString().trim().length() >= 6) {
			return true;
		}
		return false;
	}

	protected boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	class ExecuteTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String result = WSConnector.SignUp(params[0], params[1], params[2],
					params[3]);
			// PostData(params);
			return result;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			super.onPreExecute();

			pDialog.setMessage("Loading..");

			pDialog.show();

		}

		@Override
		protected void onPostExecute(String jsonobjectString) {
			// TODO Auto-generated method stub
			super.onPostExecute(jsonobjectString);
			pDialog.dismiss();
			
			try {
				JSONArray jsonArrobject = new JSONArray(jsonobjectString);
				JSONObject jsonobject = jsonArrobject.getJSONObject(0);
				if (!jsonobject.has("error")) {
					sharedPreferences();
					AlertDialog.Builder alertDialog =  new AlertDialog.Builder(SignUp_Activity.this);
					alertDialog.setTitle("Successfully Signed Up").setMessage("You have been registered successfully.Kindly confirm your email").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							Intent i = new Intent(SignUp_Activity.this,
									SignIn_Activity.class);
							startActivity(i);
							finish();
						}
					});
					AlertDialog dialog = alertDialog.create();
					dialog.show();
				}
				else {
					AlertDialog.Builder alertDialog =  new AlertDialog.Builder(SignUp_Activity.this);
					alertDialog.setMessage("An account already exists with this email address.").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
//							
						}
					});
					AlertDialog dialog = alertDialog.create();
					dialog.show();
				
				}
				 
//				} else if (jsonobject.getString("error").equals("Person already exists with this email address.")) 
//					
//				{
//					AlertDialog.Builder alertDialog =  new AlertDialog.Builder(SignUp_Activity.this);
//					alertDialog.setMessage("An account already exists with this email address.").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//						
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							// TODO Auto-generated method stub
//							dialog.dismiss();
//							
//						}
//					});
//					AlertDialog dialog = alertDialog.create();
//					dialog.show();
//				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private void sharedPreferences() {
		// TODO Auto-generated method stub
		editor = sharedPreferences.edit();
		editor.putString(AppConstant.FIRST_NAME, first_name);
		editor.putString(AppConstant.LAST_NAME, last_name);
		editor.putString(AppConstant.EMAIL, email);
		editor.putString(AppConstant.PWD, pwd);
		editor.putString(AppConstant.CONFIRM_EMAIL, confirmEmail);
		editor.putString(AppConstant.CONFIRM_PWD, confirmPwd);
		editor.commit();
		editor.clear();

	}
}
