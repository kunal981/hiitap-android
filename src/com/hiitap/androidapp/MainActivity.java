package com.hiitap.androidapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.View;

import com.hiitap.androidapp.base.BaseContainerFragment;
import com.hiitap.androidapp.base.HomeContainerFragment;
import com.hiitap.androidapp.base.ProfileConatinerFragment;
import com.hiitap.androidapp.base.RecentConatinerFragment;
import com.hiitap.androidapp.base.SearchContainerFragment;
import com.hiitap.androidapp.base.TapContainerFragment;

public class MainActivity extends FragmentActivity {

	private FragmentTabHost mTabHost;
	private static final int RESULT_OK = 0;
	private static int RESULT_LOAD_IMAGE = 1;

	private static final String TAB_HOME = "Home";
	private static final String TAB_PROFILE = "Profile";
	private static final String TAB_RECENT = "Recent";
	private static final String TAB_SEARCH = "Search";
	private static final String TAB_SEND_TAP = "Tap";

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_main);
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);

		mTabHost.setup(this, getSupportFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_HOME).setIndicator("",
						getResources().getDrawable(R.drawable.homeimg)),
				HomeContainerFragment.class, null);
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_PROFILE).setIndicator("",
						getResources().getDrawable(R.drawable.profileimg)),
				ProfileConatinerFragment.class, null);
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_RECENT).setIndicator("",
						getResources().getDrawable(R.drawable.recentimg)),
				RecentConatinerFragment.class, null);
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_SEARCH).setIndicator("",
						getResources().getDrawable(R.drawable.searchimg)),
				SearchContainerFragment.class, null);
		mTabHost.addTab(
				mTabHost.newTabSpec(TAB_SEND_TAP).setIndicator("",
						getResources().getDrawable(R.drawable.tapimg)),
						TapContainerFragment.class,null);

	}

	//
	// @Override
	// protected void onDestroy() {
	// // TODO Auto-generated method stub
	// super.onDestroy();
	// mTabHost = null;
	// }

	@Override
	public void onBackPressed() {

		boolean isPopFragment = false;
		String currentTabTag = mTabHost.getCurrentTabTag();

		if (currentTabTag.equals(TAB_HOME)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_HOME)).popFragment();
		} else if (currentTabTag.equals(TAB_PROFILE)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_PROFILE)).popFragment();
		} else if (currentTabTag.equals(TAB_RECENT)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_RECENT)).popFragment();
		} else if (currentTabTag.equals(TAB_SEARCH)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_SEARCH)).popFragment();
		} else if (currentTabTag.equals(TAB_SEND_TAP)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_SEND_TAP)).popFragment();
		}

		if (!isPopFragment) {

			if (mTabHost.getCurrentTab() == 0) {
				finish();
			} else {
				mTabHost.setCurrentTab(0);
			}
		}

	}

	public void onBackButtonClick(View v) {
		onBackPressed();
	}
	@Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  String currentTabTag = mTabHost.getCurrentTabTag();
	  ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(
	    currentTabTag)).onActivityResult(requestCode, resultCode, data);

	 }}
