package com.hiitap.androidapp.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.hiitap.androidapp.MainActivity;
import com.hiitap.androidapp.R;

public class SampleActivity extends ActionBarActivity implements
		RadioGroup.OnCheckedChangeListener, View.OnClickListener {

	SegmentedGroup segmentedgrp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_home);
		SegmentedGroup segmented4 = (SegmentedGroup) findViewById(R.id.segmented4);
		segmented4.setTintColor(getResources().getColor(
				R.color.radio_button_selected_color));

		// Set change listener on SegmentedGroup

		segmented4.setOnCheckedChangeListener(this);

	}

	public static class PlaceholderFragment extends Fragment implements
			RadioGroup.OnCheckedChangeListener, View.OnClickListener {

		SegmentedGroup segmentedgrp;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_home, container,
					false);

			SegmentedGroup segmented4 = (SegmentedGroup) rootView
					.findViewById(R.id.segmented4);
			segmented4.setTintColor(getResources().getColor(
					R.color.radio_button_selected_color));

			// Set change listener on SegmentedGroup

			segmented4.setOnCheckedChangeListener(this);

			return rootView;
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch (checkedId) {

			case R.id.button41:
				Intent i1 = new Intent(getActivity(), MainActivity.class);
				startActivity(i1);
				return;
			case R.id.button42:
				Intent i2 = new Intent(getActivity(), MainActivity.class);
				startActivity(i2);
				return;
			case R.id.button43:
				Intent i3 = new Intent(getActivity(), MainActivity.class);
				startActivity(i3);
				return;

			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.button41:
			Intent i1 = new Intent(this, MainActivity.class);
			startActivity(i1);
			return;
		case R.id.button42:
			Intent i2 = new Intent(this, MainActivity.class);
			startActivity(i2);
			return;
		case R.id.button43:
			Intent i3 = new Intent(this, MainActivity.class);
			startActivity(i3);
			return;
		}

	}
}
