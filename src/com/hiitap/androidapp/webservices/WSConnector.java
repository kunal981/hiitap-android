package com.hiitap.androidapp.webservices;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.hiitap.androidapp.AppConstant;

public class WSConnector {

	public static String authenticate( String email,
			String password) {
		// TODO Auto-generated method stub
		String url = AppConstant.BASE_URL + AppConstant.LOGIN;
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("email", email));
		pairs.add(new BasicNameValuePair("password", password));
		String result = WSAdapter.postJSONObject(url, pairs);
		return result;
		// return WSAdapter.getJSONObject(url);
	}

	public static String postSession(String token) {
		String url = AppConstant.BASE_URL + AppConstant.GET_SESSION + "?token="
				+ token;
		String result_session = WSAdapter.getJSONObject(url);
		return result_session;

	}

	public static String SignUp(String first_name, String last_name,
			String email, String password) {
		String URL = AppConstant.BASE_URL + AppConstant.SIGN_UP;
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("first_name", first_name));
		pairs.add(new BasicNameValuePair("last_name", last_name));
		pairs.add(new BasicNameValuePair("email", email));
		pairs.add(new BasicNameValuePair("password", password));
		String result = WSAdapter.postJSONObject(URL, pairs);
		return result;

	}

	public static String getProfile(String uuid, String token) {
		String URL = AppConstant.BASE_URL + AppConstant.GET_PROFILE + "uuid="
				+ uuid + "&" + "token=" + token;
		String result_profile = WSAdapter.getJSONObject(URL);
		return result_profile;

	}

	public static String getPopularPeople(String uuid, String token) {
		String URL = AppConstant.BASE_URL + AppConstant.GET_POPULAR_PEOPLE
				+ "uuid=" + uuid + "&" + "token=" + token;
		String result_getPopularpeople = WSAdapter.getJSONObject(URL);
		return result_getPopularpeople;

	}

	public static String getPopularPosts(String uuid, String token) {
		String URL = AppConstant.BASE_URL + AppConstant.GET_POPULAR_POSTS
				+ "uuid=" + uuid + "&" + "token=" + token;
		String result_getPopularposts = WSAdapter.getJSONObject(URL);
		return result_getPopularposts;

	}

	public static String getProfilePosts(String uuid, String token) {
		String Url = AppConstant.BASE_URL + AppConstant.GET_PROFILE_POSTS
				+ "uuid=" + uuid + "&" + "token=" + token;
		String result = WSAdapter.getJSONObject(Url);
		return result;

	}

	public static String getFollower(String uuid, String token) {
		String Url = AppConstant.BASE_URL + AppConstant.GET_FOLLOWERS + "uuid="
				+ uuid + "&" + "token=" + token;

		String resultFollower = WSAdapter.getJSONObject(Url);
		return resultFollower;

	}

	public static String getRecentFeeds(String uuid, String token) {
		String url = AppConstant.BASE_URL + AppConstant.RECENT_FEEDS + "uuid="
				+ uuid + "&" + "token=" + token;
		String resultRecent = WSAdapter.getJSONObject(url);
		return resultRecent;

	}

	public static String getFollowing(String uuid, String token) {
		String URL = AppConstant.BASE_URL + AppConstant.GET_FOLLOWING + "uuid="
				+ uuid + "&" + "token=" + token;
		return WSAdapter.getJSONObject(URL);

	}

	public static String getSearch(String uuid, String token, String query) {
		String Url = AppConstant.BASE_URL + AppConstant.SEARCH + "uuid=" + uuid
				+ "&" + "token=" + token + "&" + "type=person" + "&" + "query="
				+ query;

		String result_search = WSAdapter.getJSONObject(Url);
		return result_search;

	}

	public static String getLogout(String uuid, String token) {
		String URL = AppConstant.BASE_URL + AppConstant.LOGOUT + "uuid=" + uuid
				+ "&" + "token=" + token;
		String result_logout = WSAdapter.getJSONObject(URL);
		return result_logout;

	}

	public static String getLikePosts(String uuid, String token) {
		String Url = AppConstant.BASE_URL + AppConstant.GET_POST_PROFILE_INFO
				+ "uuid=" + uuid + "&" + "token=" + token;
		String result_post = WSAdapter.getJSONObject(Url);

		return result_post;

	}

	public static String getCommentPosts(String uuid, String token) {
		String Url = AppConstant.BASE_URL + AppConstant.COMMENT_POSTS
				+ "token=" + token + "&" + "post=" + uuid;
		String result_post_comment = WSAdapter.getJSONObject(Url);

		return result_post_comment;
	}

	public static String LikePosts(String token, String uuid, String person,
			String like) {
		String URL = AppConstant.BASE_URL + AppConstant.LIKE_POSTS + "token="
				+ token + "&" + "post=" + uuid + "&" + "person=" + person + "&"
				+ "like=" + like;
		String result_like_post = WSAdapter.getJSONObject(URL);
		return result_like_post;

	}

	public static String TAP_Follow(String uuid, String token, String follow) {
		String Url = AppConstant.BASE_URL + AppConstant.TAP_FOLLOW + "token="
				+ token + "&" + "person=" + uuid + "&" + "follow=" + follow;
		String result_tap_follow = WSAdapter.getJSONObject(Url);
		return result_tap_follow;

	}

	public static String postComment(String token, String uuid, String person,
			String message) {
		String Url = AppConstant.BASE_URL + AppConstant.COMMENT + "token="
				+ token + "&" + "post=" + uuid + "&" + "person=" + person + "&"
				+ "message=" + message;
		String result_postComment = WSAdapter.getJSONObject(Url);
		return result_postComment;

	}

	public static String sendFeedback(String token, String subject,
			String details) {
		String URL = AppConstant.BASE_URL + AppConstant.POST_FEEDBACK;
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("token", token));
		pairs.add(new BasicNameValuePair("subject", subject));
		pairs.add(new BasicNameValuePair("details", details));

		String result_feedback = WSAdapter.postJSONObject(URL, pairs);
		return result_feedback;

	}

	public static String postMessage(String token, String to_user,
			String action, String message, String image, String image_type) {
		String Url = AppConstant.BASE_URL + AppConstant.POST_MESSAGE;
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("token", token));
		pairs.add(new BasicNameValuePair("to", to_user));
		pairs.add(new BasicNameValuePair("action", action));
		pairs.add(new BasicNameValuePair("message", message));
		pairs.add(new BasicNameValuePair("image", image));
		pairs.add(new BasicNameValuePair("image_type", image_type));

		String result_message = WSAdapter.postJSONObject(Url, pairs);

		return result_message;

	}

	public static String resetpassword(String email) {
		String URL = AppConstant.BASE_URL + AppConstant.REQUEST_PASSWORD
				+ "email=" + email;
		String result = WSAdapter.getJSONObject(URL);
		return result;

	}

	public static String postmessageEmail(String first_name, String last_name,
			String email, String message) {
		String URL = AppConstant.BASE_URL + AppConstant.POST_MESSAGE
				+ "first_name=" + first_name + "&" + "last_name=" + last_name
				+ "&" + "action=support" + "&" + "email=" + email + "&"
				+ "message=" + message;

		String result = WSAdapter.getJSONObject(URL);
		return result;

	}

	public static String postReport(String uuid, String message_uuid,
			String message) {
		String Url = AppConstant.BASE_URL + AppConstant.POST_REPORT + "uuid="
				+ uuid + "&" + "message=" + message_uuid + "&" + "message_uuid"
				+ message;
		String result = WSAdapter.getJSONObject(Url);
		return result;

	}
	public static String GetPopularCountries(){
		String URL = AppConstant.BASE_URL+AppConstant.GET_POPULAR_COUNTRIES;
		String result_popular_countries = WSAdapter.getJSONObject(URL);
		return result_popular_countries;
		
	}
	
	public static String GetPopularCities(String country){
		String Url = AppConstant.BASE_URL+AppConstant.GET_POPULAR_CITIES+"country="+country;
		String result_popular_cities = WSAdapter.getJSONObject(Url);
		return result_popular_cities;
		
	}
	
	
	public static String GetMessageCount(){
		String url= AppConstant.BASE_URL+AppConstant.GET_MESSAGE_COUNT;
		String result_count = WSAdapter.getJSONObject(url);
		return result_count;
		
	}
	
	
	
	public static String SaveProfilePic(String uuid,String token,String data,String type){
		String URL = AppConstant.BASE_URL+AppConstant.SAVE_PROFILE_PIC;
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("uuid", uuid));
		pairs.add(new BasicNameValuePair("token", token));
		pairs.add(new BasicNameValuePair("data", data));
		pairs.add(new BasicNameValuePair("type", type));
		String result_updateprofilepic = WSAdapter.postJSONObject(URL, pairs);
		
		return result_updateprofilepic;
		
	}
}
