package com.hiitap.androidapp;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.hiitap.androidapp.home.AboutActivity;

public class WelcomePage_Activity extends Activity  {
	Button btnsignin, btnsignup,btn_about,btn_refresh;
	static final LatLng HAMBURG = new LatLng(49.2756d, -123.12717d);
	static final LatLng KIEL = new LatLng(53.551, 9.993);
	private GoogleMap map;
//	RadioButton radioButton1, radioButton2, radioButton3;
//	ExpandableListView expandableListView;
//	ListView listView_mostLiked,listView_mostThanked;
//	List<MostThankedModel> mostThankedModels;
//	List<UserModal> userModals;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcomepage);
//		userModals = new ArrayList<UserModal>();
//		mostThankedModels = new ArrayList<MostThankedModel>();
//		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
//				.getMap();
//		Marker hamburg = map.addMarker(new MarkerOptions().position(HAMBURG)
//				.title("Hamburg"));
//
//		// Move the camera instantly to hamburg with a zoom of 15.
//		map.moveCamera(CameraUpdateFactory.newLatLngZoom(HAMBURG, 15));
//
//		// Zoom in, animating the camera.
//		map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

		btnsignin = (Button) findViewById(R.id.btn_sign_in);
		btnsignup = (Button) findViewById(R.id.btn_sign_up);
		btn_about = (Button) findViewById(R.id.btn_About_welcome);
		btn_refresh = (Button)findViewById(R.id.btn_title_Refesh);
		btnsignin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent isignin = new Intent(WelcomePage_Activity.this,
						SignIn_Activity.class);
				startActivity(isignin);
				finish();

			}
		});

		btnsignup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent isignUp = new Intent(WelcomePage_Activity.this,
						SignUp_Activity.class);
				startActivity(isignUp);
				finish();

			}
		});
		btn_about.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent iAbout = new Intent(WelcomePage_Activity.this, AboutActivity.class);
				startActivity(iAbout);
				
			}
		});

		btn_refresh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Refreshing..", Toast.LENGTH_SHORT).show();
			}
		});
		
//		expandableListView = (ExpandableListView)findViewById(R.id.expandableListView_welcome);
//		listView_mostLiked = (ListView)findViewById(R.id.listview_thanked);
//		listView_mostThanked = (ListView)findViewById(R.id.listview_mostthanked);
//		listView_mostThanked.setVisibility(View.GONE);
//		listView_mostLiked.setVisibility(View.GONE);
//		
//		SegmentedGroup segmented4 = (SegmentedGroup)findViewById(R.id.segmented4);
//		segmented4.setTintColor(getResources().getColor(
//				R.color.radio_button_selected_color));
//		radioButton1 = (RadioButton)findViewById(R.id.button41);
//		radioButton1.setChecked(true);
//		radioButton2 = (RadioButton)findViewById(R.id.button42);
//		radioButton3 = (RadioButton)findViewById(R.id.button43);
//		
//		
//		
//		segmented4.setOnCheckedChangeListener(this);
//		
//		
//		
//		radioButton2.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				if (userModals.size() == 0) {
////					new ExcecuteTask_Mostthanked().execute(
////							AppConstant.BASE_URL,
////							AppConstant.GET_POPULAR_PEOPLE);
//				}
//
//			}
//		});
//		radioButton3.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
////				if (mostThankedModels.size() == 0) {
////					new ExecuteTask_Mostliked().execute(AppConstant.BASE_URL,
////							AppConstant.GET_POPULAR_POSTS);
//				//}
//
//			}
//		});
//		
//		
//		
//	}
//	
//
//	@Override
//	public void onCheckedChanged(RadioGroup group, int checkedId) {
//		// TODO Auto-generated method stub
//		switch (checkedId) {
//		case R.id.button41:
//			radioButton1.setSelected(true);
//			initTopCountriesList();
//
//			return;
//		case R.id.button42:
//			initMostThankedList();
//
//			return;
//		case R.id.button43:
//
//			initMostLikedList();
//
//			return;
//
//		}
//		
//	}
//	
//	private void initMostThankedList() {
//		expandableListView.setVisibility(View.GONE);
//		listView_mostLiked.setVisibility(View.GONE);
//		listView_mostThanked.setVisibility(View.VISIBLE);
//
//	}
//
//	private void initMostLikedList() {
//		expandableListView.setVisibility(View.GONE);
//		listView_mostLiked.setVisibility(View.VISIBLE);
//		listView_mostThanked.setVisibility(View.GONE);
//
//	}
//
//	private void initTopCountriesList() {
//
//		expandableListView.setVisibility(View.VISIBLE);
//		listView_mostLiked.setVisibility(View.GONE);
//		listView_mostThanked.setVisibility(View.GONE);
//
//	}


}}
