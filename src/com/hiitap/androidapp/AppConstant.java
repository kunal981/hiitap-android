package com.hiitap.androidapp;

public class AppConstant {
	public static final String HII_TAP = "hii_tap";
	public static final String USERNAME = "Username";
	public static final String PASSWORD = "password";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String EMAIL = "email";
	public static final String CONFIRM_EMAIL = "confirmEmail";
	public static final String PWD = "pwd";
	public static final String CONFIRM_PWD = "confirmPwd";
	public static final String POST = "post";
	public static final String TOKEN = "token";
	public static final String BASE_URL = "https://www.hiitap.com/api/";
	public static final String LOGIN = "Authenticate?";
	public static final String GET_SESSION = "GetSession";
	public static final String UUID = "uuid";
	public static final String GET_PROFILE = "GetProfile?";
	public static final String GET_PROFILE_POSTS = "GetProfilePosts?";
	public static final String GET_FOLLOWERS = "GetFollowers?";
	public static final String GET_FOLLOWING = "GetFollowing?";
	public static final String RECENT_FEEDS = "GetNewsFeed?";
	public static final String SEARCH = "Search?";
	public static final String LOGOUT = "InvalidateSession?";
	public static final String TO_USER = "to";
	public static final String FROM_USER = "from";
	public static final String GET_POST_PROFILE_INFO = "GetPostLikeInformation?";
	public static final String COMMENT_POSTS = "GetPostComments?";
	public static final String LIKE_POSTS = "LikePost?";
	public static final String TAP_FOLLOW = "Follow?";
	public static final String GET_POPULAR_PEOPLE = "GetPopularPeople?";
	public static final String GET_POPULAR_POSTS = "GetPopularPosts?";
	public static final String COMMENT= "Comment?";
	public static final String POST_FEEDBACK= "RaiseSupportQuery?";
	public static final String POST_MESSAGE ="PostMessage";
	public static final String SIGN_UP = "SignUp";
	public static final String REQUEST_PASSWORD = "RequestPasswordResetEmail?";
	public static final String POST_EMAIL = "PostEmailMessage?";
	public static final String POST_REPORT = "GetMessage?";
	public static final String GET_POPULAR_COUNTRIES = "GetPopularCountries?";
	public static final String GET_POPULAR_CITIES = "GetPopularCities?";
	public static final String SAVE_PROFILE_PIC = "SaveProfilePic";
	public static final String GET_MESSAGE_COUNT = "GetMessageCount?";

}
